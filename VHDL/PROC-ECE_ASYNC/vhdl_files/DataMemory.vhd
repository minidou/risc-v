-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM
-- Data Memory VHDL

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity DataMemory is
    port (
        -- INPUTS
        DMclock       : in std_logic;
        DMstore       : in std_logic;
        DMload        : in std_logic;
        DMaddr        : in std_logic_vector(31 downto 0);
        DMin          : in std_logic_vector(31 downto 0);
        DMfunct3      : in std_logic_vector(2 downto 0);
        -- OUTPUTS
        DMout         : out std_logic_vector(31 downto 0)
    );
end entity;

-- ARCHITECTURE
architecture archi of DataMemory is

    component RAM00 is
    PORT
    (
--        clock     : IN STD_LOGIC  := '1';     
        inclock     : IN STD_LOGIC  := '1';
        outclock    : IN STD_LOGIC  := '0';
        rden        : IN STD_LOGIC  := '1';
        wren        : IN STD_LOGIC  := '1';
        data        : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        address     : IN STD_LOGIC_VECTOR (9 DOWNTO 0);
        byteena     : IN STD_LOGIC_VECTOR (3 DOWNTO 0);
        q           : OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
    );
    end component;
    -- SIGNALS RAM
    
    signal SIGaddrShift : STD_LOGIC_VECTOR (31 DOWNTO 0);
    signal SIGDMout     : STD_LOGIC_VECTOR (31 DOWNTO 0);
    signal SIGDataMout  : STD_LOGIC_VECTOR (31 DOWNTO 0);
    signal SIGbyteena   : STD_LOGIC_VECTOR (3 DOWNTO 0);
    signal SIGClockInv  : STD_LOGIC;
    signal SIGwren      : STD_LOGIC;

begin

    SIGwren <= DMstore and not DMaddr(31);
     
    SIGaddrShift <= std_logic_vector(shift_right(unsigned(DMaddr), 2)) WHEN (DMload = '1' or SIGwren = '1');
     
    SIGClockInv <= not DMclock;

    SIGbyteena <= "0001" WHEN (DMfunct3(1 downto 0) = "00") ELSE
                  "0011" WHEN (DMfunct3(1 downto 0) = "01") ELSE
                  "1111" WHEN (DMfunct3(1 downto 0) = "10");
                  
    SIGDataMout <= (SigDMout AND x"000000ff") WHEN (DMfunct3 = "100") ELSE
                   (SigDMout AND x"0000ffff") WHEN (DMfunct3 = "101") ELSE
                   (SigDMout AND x"ffffffff") WHEN (DMfunct3 = "010") ELSE
                   (SigDMout AND x"000000ff") WHEN (DMfunct3 = "000" AND SigDMout(7) = '0') ELSE
                   (SigDMout OR  x"ffffff00") WHEN (DMfunct3 = "000" AND SigDMout(7) = '1') ELSE
                   (SigDMout AND x"0000ffff") WHEN (DMfunct3 = "001" AND SigDMout(15) = '0') ELSE
                   (SigDMout OR  x"ffff0000") WHEN (DMfunct3 = "001" AND SigDMout(15) = '1') ELSE
                   -- (others => '0');
                   x"0000DEAD";

    DMout <= SIGDataMout when DMload = '1';
    -- INSTANCES
    instRAM00 : RAM00
    port map(
        data => DMin,
--        clock => DMclock,
        inclock => DMclock,
        outclock => SIGClockInv,
        address => SIGaddrShift(9 downto 0),
        rden => '1',
        byteena => SIGbyteena,
        wren => SIGwren,
        q => SigDMout
    );

    -- END
end archi;
-- END FILE