-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity TestBenchTop is
end entity;


architecture VHDL of TestBenchTop is
	component Top is
		port (
		-- INPUTS
		TOPclock		: in std_logic;		--must go through pll
		TOPreset		: in std_logic; 	--SW0
		TOPresetIM	: in std_logic; 		--SW1
		-- DEMO OUTPUTS
		TOPcounter : inout std_logic_vector(31 downto 0); --0x80000001
		TOPdisplay1 : inout std_logic_vector(31 downto 0);--0x80000002
		TOPdisplay2 : inout std_logic_vector(31 downto 0);--0x80000003
		TOPleds : inout std_logic_vector(31 downto 0) 	--0x80000004
	);
	end component;

signal SigTOPclock : std_logic;
signal SigTOPreset : std_logic;
signal SigTOPresetIM : std_logic;
signal SigTOPcounter : std_logic_vector (31 downto 0);
signal SigTOPdisplay1 : std_logic_vector (31 downto 0);
signal SigTOPdisplay2 : std_logic_vector (31 downto 0);
signal SigTOPleds: std_logic_vector (31 downto 0);

	begin
	clk : process
	begin
		SigTOPclock <= '1';
		wait for 5 ns;
		SigTOPclock <= '0';
		wait for 5 ns;
	end process;
	
	R1 : process
	begin
		SigTOPreset <= '1';
		wait for 10 ns;
		SigTOPreset <= '0';
		wait;

	end process;
	
	R2 : process
	begin
		SigTOPresetIM <= '0';
		wait;
	end process;
	


		
		
		-- TestDisplay
	

	i1 : Top port map(TOPclock => SigTOPclock, TOPreset => SigTOPreset, TOPresetIM => SigTOPresetIM, TOPcounter => SigTOPcounter, 
							TOPdisplay1 => SigTOPdisplay1, TOPdisplay2 => SigTOPdisplay2, TOPleds => SigTOPleds);

end VHDL;