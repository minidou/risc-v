-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM
-- Instruction Memory VHDL

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity InstructionMemory is
	port (
		-- INPUTS
		IMclock			: in std_logic;
		IMreset			: in std_logic;
		IMprogcounter	: in std_logic_vector(31 downto 0);
		-- OUTPUTS
		IMout				: out std_logic_vector(31 downto 0)
	);
end entity;

-- ARCHITECTURE
architecture archi of InstructionMemory is
	type mem is array(0 to 5) of std_logic_vector(31 downto 0);
	signal SigIMmemory : mem :=(

 x"00300093",  -- 0 ADDI reg0+#3=>reg1
 x"00108093",  -- 4 ADDI reg1+1=>reg1
 x"00402103",  -- 8 LoadW (reg0+#4)=>reg2
 x"00802183",  -- C LoadW (reg0+#8)=>reg3
 x"00108093",  -- 10 ADDI reg1+1=>reg1
-- x"FEFFF26F"   -- 14 JAL -18
 x"FEDFF26F"   -- 14 JAL -14
 );
   
	signal sigad : integer;
	signal sigpc : std_logic_vector(2 downto 0);
begin
	-- BEGIN
	
	sigpc <= IMprogcounter(4 downto 2);
	
	IMout <= SigIMmemory(sigad) when rising_edge(IMclock);
	
	
	Sigad <= 0 when ((unsigned(sigpc) > 5)) else to_integer(unsigned(sigpc));
end archi;
-- END FILE

