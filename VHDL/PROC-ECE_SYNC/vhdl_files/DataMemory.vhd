-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM
-- Data Memory VHDL

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity DataMemory is
	port (
		-- INPUTS
		DMclock		: in std_logic;
		DMreset		: in std_logic;
		DMstore		: in std_logic;
		DMload		: in std_logic;
		DMaddr		: in std_logic_vector(31 downto 0);
		DMin			: in std_logic_vector(31 downto 0);
		DMfunct3		: in std_logic_vector(2 downto 0);
		-- OUTPUTS
		DMout			: out std_logic_vector(31 downto 0)
	);
end entity;

-- ARCHITECTURE
architecture archi of DataMemory is
	type mem is array(0 to 15) of std_logic_vector(7 downto 0);
	signal SigDMmemory : mem := (
	x"11", x"22", x"33", x"44",
	x"55", x"66", x"77", x"88",
	x"99", x"AA", x"BB", x"CC",
	x"DD", x"EE", x"FF", x"00" );
	signal SigDMaddr00 : integer;
	signal SigDMaddr08 : integer;
	signal SigDMaddr16 : integer;
	signal SigDMaddr24 : integer;
	
	signal sigdmout : std_logic_vector(31 downto 0);
	
begin
	-- BEGIN
	
	
	
	-- useful adresses used to store or load
	SigDMaddr00 <= to_integer(unsigned(DMaddr)) when (DMload = '1' OR DMstore = '1') else
						0;
	SigDMaddr08 <= to_integer(unsigned(DMaddr)) + 1 when (DMload = '1' OR DMstore = '1') else
						0;
	SigDMaddr16 <= to_integer(unsigned(DMaddr)) + 2 when (DMload = '1' OR DMstore = '1') else
						0;
	SigDMaddr24 <= to_integer(unsigned(DMaddr)) + 3 when (DMload = '1' OR DMstore = '1') else
						0;
	
	-- store for synchronous data memory
	p2 : process(DMclock, DMreset)
	begin
		if(rising_edge(DMclock)) then
			if(DMstore = '1') then
				if(DMfunct3 = "000") then
					SigDMmemory(SigDMaddr00) <= DMin(7 downto 0);
				elsif(DMfunct3 = "001") then
					SigDMmemory(SigDMaddr00) <= DMin(7 downto 0);
					SigDMmemory(SigDMaddr08) <= DMin(15 downto 8);
					
				elsif(DMfunct3 = "010") then
					SigDMmemory(SigDMaddr00) <= DMin(7 downto 0);
					SigDMmemory(SigDMaddr08) <= DMin(15 downto 8);
					SigDMmemory(SigDMaddr16) <= DMin(23 downto 16);
					SigDMmemory(SigDMaddr24) <= DMin(31 downto 24);
				end if;
			end if;
		end if;
	end process;
	
	
	DMout <= SigDMout when rising_edge(DMclock);
	
	-- load for asynchronous data memory
	SigDMout(7 downto 0) <= SigDMmemory(SigDMaddr00) when (DMload = '1') else
								(others => '0');
								
	SigDMout(15 downto 8)<= SigDMmemory(SigDMaddr08) when (DMload = '1' AND (DMfunct3 = "001" OR DMfunct3 = "010" OR DMfunct3 = "101")) else
								(others => '1') when (DMload = '1' AND (DMfunct3 = "000" AND ((SigDMmemory(SigDMaddr00) AND "10000000") = "10000000"))) else
								(others => '0');
	
	SigDMout(23 downto 16)<=SigDMmemory(SigDMaddr16) when (DMload = '1' AND (DMfunct3 = "010")) else
								(others => '1') when (DMload = '1' AND ((DMfunct3 = "000" AND ((SigDMmemory(SigDMaddr00) AND "10000000") = "10000000")) OR (DMfunct3 = "001" AND ((SigDMmemory(SigDMaddr08) AND "10000000") = "10000000")))) else
								(others => '0');
	
	SigDMout(31 downto 24)<=SigDMmemory(SigDMaddr24) when (DMload = '1' AND (DMfunct3 = "010")) else
								(others => '1') when (DMload = '1' AND ((DMfunct3 = "000" AND ((SigDMmemory(SigDMaddr00) AND "10000000") = "10000000")) OR (DMfunct3 = "001" AND ((SigDMmemory(SigDMaddr08) AND "10000000") = "10000000")))) else
								(others => '0');
	
	-- END
end archi;
-- END FILE