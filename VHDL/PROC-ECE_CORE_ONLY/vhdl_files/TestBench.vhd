library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.simulPkg.all;

entity testbench is
end testbench;

architecture vhdl of testbench is

    component Processor is
        Port (
        -- INPUTS
        PROCclock        : in std_logic;
        PROCreset        : in std_logic;
        PROCinstruction  : in std_logic_vector(31 downto 0);
        PROCoutputDM     : in std_logic_vector(31 downto 0);
        -- OUTPUTS
        PROCprogcounter  : out std_logic_vector(31 downto 0);
        PROCstore        : out std_logic;
        PROCload         : out std_logic;
        PROCfunct3       : out std_logic_vector(2 downto 0);
        PROCaddrDM       : out std_logic_vector(31 downto 0);
        PROCinputDM      : out std_logic_vector(31 downto 0)
    );
    end component;

    signal reset, ck : std_logic;
    signal counter, progcounter, instr: std_logic_vector(31 downto 0);
    
    signal dataAddr: std_logic_vector(31 downto 0);
    signal load, store : std_logic;
    signal dataLength : std_logic_vector(2 downto 0);
    signal inputData, outputData: std_logic_vector(31 downto 0);
    
    signal reg00, reg01, reg02, reg03, reg04, reg05, reg06, reg07, reg08, reg09, reg0A, reg0B, reg0C, reg0D, reg0E, reg0F, reg10, reg11, reg12, reg13, reg14, reg15, reg16, reg17, reg18, reg19, reg1A, reg1B, reg1C, reg1D, reg1E, reg1F : std_logic_vector(31 downto 0);
    signal SigTOPdisplay1, SigTOPdisplay2 : std_logic_vector (31 downto 0);
    
    BEGIN

    --instanciation de l'entité PROC
    iProcessor : Processor port map (
        PROCclock        => ck,
        PROCreset        => reset,
        PROCinstruction  => instr,
        PROCoutputDM     => outputData,
        PROCprogcounter  => progcounter,
        PROCstore        => store,
        PROCload         => load,
        PROCfunct3       => dataLength,
        PROCaddrDM       => dataAddr,
        PROCinputDM      => inputData
    );
    
    reg00       <= PKG_reg00;
    reg01       <= PKG_reg01;
    reg02       <= PKG_reg02;
    reg03       <= PKG_reg03;
    reg04       <= PKG_reg04;
    reg05       <= PKG_reg05;
    reg06       <= PKG_reg06;
    reg07       <= PKG_reg07;
    reg08       <= PKG_reg08;
    reg09       <= PKG_reg09;
    reg0A       <= PKG_reg0A;
    reg0B       <= PKG_reg0B;
    reg0C       <= PKG_reg0C;
    reg0D       <= PKG_reg0D;
    reg0E       <= PKG_reg0E;
    reg0F       <= PKG_reg0F;
    reg10       <= PKG_reg10;
    reg11       <= PKG_reg11;
    reg12       <= PKG_reg12;
    reg13       <= PKG_reg13;
    reg14       <= PKG_reg14;
    reg15       <= PKG_reg15;
    reg16       <= PKG_reg16;
    reg17       <= PKG_reg17;
    reg18       <= PKG_reg18;
    reg19       <= PKG_reg19;
    reg1A       <= PKG_reg1A;
    reg1B       <= PKG_reg1B;
    reg1C       <= PKG_reg1C;
    reg1D       <= PKG_reg1D;
    reg1E       <= PKG_reg1E;
    reg1F       <= PKG_reg1F;
    
 	VecteurTest : process
		begin
		-- init  simulation
			ck <= '0';
			reset <= '1';
			instr <= x"00000000"; -- init
			wait for 10 ns;
			ck <= '1';
			wait for 5 ns;
			reset <= '0';
			wait for 5 ns;
			assert false report "Index;Instruction;Description;Status;Note" severity note;

		-- load instruction 0
			ck <= '0';
			instr <= x"00c0006f"; -- JAL : reg[00] = PC+4 and PC = 0x0 + 12
			wait for 5 ns;
			assert false report "0;0x00c0006f;JAL : reg[00] = PC+4 and PC = 0x0 + 12;OK; ;" severity note;
			assert progcounter = x"00000000" report "progcounter error at step 0" severity error;
			wait for 5 ns;

		-- execute instruction 0
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 1" severity error;
			assert reg01 = x"00000000" report "reg01 error at step 1" severity error;
			assert reg02 = x"00000000" report "reg02 error at step 1" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 1" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 1" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 1" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 1" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 1" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 1" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 1" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 1" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 1" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 1" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 1" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 1" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 1" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 1" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 1" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 1" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 1" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 1" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 1" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 1" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 1" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 1" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 1" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 1" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 1" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 1" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 1" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 1" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 1" severity error;
			assert progcounter = x"0000000c" report "progcounter error at step 1" severity error;
			wait for 5 ns;

		-- load instruction 1
			ck <= '0';
			instr <= x"00000037"; -- LUI : reg[00] = 0x0 << 12
			wait for 5 ns;
			assert false report "1;0x00000037;LUI : reg[00] = 0x0 << 12;OK; ;" severity note;
			assert progcounter = x"0000000c" report "progcounter error at step 2" severity error;
			wait for 5 ns;

		-- execute instruction 1
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 3" severity error;
			assert reg01 = x"00000000" report "reg01 error at step 3" severity error;
			assert reg02 = x"00000000" report "reg02 error at step 3" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 3" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 3" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 3" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 3" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 3" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 3" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 3" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 3" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 3" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 3" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 3" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 3" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 3" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 3" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 3" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 3" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 3" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 3" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 3" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 3" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 3" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 3" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 3" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 3" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 3" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 3" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 3" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 3" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 3" severity error;
			assert progcounter = x"00000010" report "progcounter error at step 3" severity error;
			wait for 5 ns;

		-- load instruction 2
			ck <= '0';
			instr <= x"000010b7"; -- LUI : reg[01] = 0x1 << 12
			wait for 5 ns;
			assert false report "2;0x000010b7;LUI : reg[01] = 0x1 << 12;OK; ;" severity note;
			assert progcounter = x"00000010" report "progcounter error at step 4" severity error;
			wait for 5 ns;

		-- execute instruction 2
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 5" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 5" severity error;
			assert reg02 = x"00000000" report "reg02 error at step 5" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 5" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 5" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 5" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 5" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 5" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 5" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 5" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 5" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 5" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 5" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 5" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 5" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 5" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 5" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 5" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 5" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 5" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 5" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 5" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 5" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 5" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 5" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 5" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 5" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 5" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 5" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 5" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 5" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 5" severity error;
			assert progcounter = x"00000014" report "progcounter error at step 5" severity error;
			wait for 5 ns;

		-- load instruction 3
			ck <= '0';
			instr <= x"000ff137"; -- LUI : reg[02] = 0xff << 12
			wait for 5 ns;
			assert false report "3;0x000ff137;LUI : reg[02] = 0xff << 12;OK; ;" severity note;
			assert progcounter = x"00000014" report "progcounter error at step 6" severity error;
			wait for 5 ns;

		-- execute instruction 3
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 7" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 7" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 7" severity error;
			assert reg03 = x"00000000" report "reg03 error at step 7" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 7" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 7" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 7" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 7" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 7" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 7" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 7" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 7" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 7" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 7" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 7" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 7" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 7" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 7" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 7" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 7" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 7" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 7" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 7" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 7" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 7" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 7" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 7" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 7" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 7" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 7" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 7" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 7" severity error;
			assert progcounter = x"00000018" report "progcounter error at step 7" severity error;
			wait for 5 ns;

		-- load instruction 4
			ck <= '0';
			instr <= x"0ffff1b7"; -- LUI : reg[03] = 0xffff << 12
			wait for 5 ns;
			assert false report "4;0x0ffff1b7;LUI : reg[03] = 0xffff << 12;OK; ;" severity note;
			assert progcounter = x"00000018" report "progcounter error at step 8" severity error;
			wait for 5 ns;

		-- execute instruction 4
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 9" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 9" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 9" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 9" severity error;
			assert reg04 = x"00000000" report "reg04 error at step 9" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 9" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 9" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 9" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 9" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 9" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 9" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 9" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 9" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 9" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 9" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 9" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 9" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 9" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 9" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 9" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 9" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 9" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 9" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 9" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 9" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 9" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 9" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 9" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 9" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 9" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 9" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 9" severity error;
			assert progcounter = x"0000001c" report "progcounter error at step 9" severity error;
			wait for 5 ns;

		-- load instruction 5
			ck <= '0';
			instr <= x"fffff237"; -- LUI : reg[04] = 0xfffff << 12
			wait for 5 ns;
			assert false report "5;0xfffff237;LUI : reg[04] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000001c" report "progcounter error at step 10" severity error;
			wait for 5 ns;

		-- execute instruction 5
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 11" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 11" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 11" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 11" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 11" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 11" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 11" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 11" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 11" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 11" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 11" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 11" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 11" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 11" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 11" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 11" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 11" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 11" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 11" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 11" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 11" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 11" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 11" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 11" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 11" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 11" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 11" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 11" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 11" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 11" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 11" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 11" severity error;
			assert progcounter = x"00000020" report "progcounter error at step 11" severity error;
			wait for 5 ns;

		-- load instruction 6
			ck <= '0';
			instr <= x"fffff2b7"; -- LUI : reg[05] = 0xfffff << 12
			wait for 5 ns;
			assert false report "6;0xfffff2b7;LUI : reg[05] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000020" report "progcounter error at step 12" severity error;
			wait for 5 ns;

		-- execute instruction 6
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 13" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 13" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 13" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 13" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 13" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 13" severity error;
			assert reg06 = x"00000000" report "reg06 error at step 13" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 13" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 13" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 13" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 13" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 13" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 13" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 13" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 13" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 13" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 13" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 13" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 13" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 13" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 13" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 13" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 13" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 13" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 13" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 13" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 13" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 13" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 13" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 13" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 13" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 13" severity error;
			assert progcounter = x"00000024" report "progcounter error at step 13" severity error;
			wait for 5 ns;

		-- load instruction 7
			ck <= '0';
			instr <= x"fffff337"; -- LUI : reg[06] = 0xfffff << 12
			wait for 5 ns;
			assert false report "7;0xfffff337;LUI : reg[06] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000024" report "progcounter error at step 14" severity error;
			wait for 5 ns;

		-- execute instruction 7
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 15" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 15" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 15" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 15" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 15" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 15" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 15" severity error;
			assert reg07 = x"00000000" report "reg07 error at step 15" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 15" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 15" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 15" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 15" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 15" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 15" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 15" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 15" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 15" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 15" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 15" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 15" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 15" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 15" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 15" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 15" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 15" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 15" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 15" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 15" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 15" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 15" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 15" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 15" severity error;
			assert progcounter = x"00000028" report "progcounter error at step 15" severity error;
			wait for 5 ns;

		-- load instruction 8
			ck <= '0';
			instr <= x"fffff3b7"; -- LUI : reg[07] = 0xfffff << 12
			wait for 5 ns;
			assert false report "8;0xfffff3b7;LUI : reg[07] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000028" report "progcounter error at step 16" severity error;
			wait for 5 ns;

		-- execute instruction 8
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 17" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 17" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 17" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 17" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 17" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 17" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 17" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 17" severity error;
			assert reg08 = x"00000000" report "reg08 error at step 17" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 17" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 17" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 17" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 17" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 17" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 17" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 17" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 17" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 17" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 17" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 17" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 17" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 17" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 17" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 17" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 17" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 17" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 17" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 17" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 17" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 17" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 17" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 17" severity error;
			assert progcounter = x"0000002c" report "progcounter error at step 17" severity error;
			wait for 5 ns;

		-- load instruction 9
			ck <= '0';
			instr <= x"fffff437"; -- LUI : reg[08] = 0xfffff << 12
			wait for 5 ns;
			assert false report "9;0xfffff437;LUI : reg[08] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000002c" report "progcounter error at step 18" severity error;
			wait for 5 ns;

		-- execute instruction 9
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 19" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 19" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 19" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 19" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 19" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 19" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 19" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 19" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 19" severity error;
			assert reg09 = x"00000000" report "reg09 error at step 19" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 19" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 19" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 19" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 19" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 19" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 19" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 19" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 19" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 19" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 19" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 19" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 19" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 19" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 19" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 19" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 19" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 19" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 19" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 19" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 19" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 19" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 19" severity error;
			assert progcounter = x"00000030" report "progcounter error at step 19" severity error;
			wait for 5 ns;

		-- load instruction 10
			ck <= '0';
			instr <= x"fffff4b7"; -- LUI : reg[09] = 0xfffff << 12
			wait for 5 ns;
			assert false report "10;0xfffff4b7;LUI : reg[09] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000030" report "progcounter error at step 20" severity error;
			wait for 5 ns;

		-- execute instruction 10
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 21" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 21" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 21" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 21" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 21" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 21" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 21" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 21" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 21" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 21" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 21" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 21" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 21" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 21" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 21" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 21" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 21" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 21" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 21" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 21" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 21" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 21" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 21" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 21" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 21" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 21" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 21" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 21" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 21" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 21" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 21" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 21" severity error;
			assert progcounter = x"00000034" report "progcounter error at step 21" severity error;
			wait for 5 ns;

		-- load instruction 11
			ck <= '0';
			instr <= x"fffff537"; -- LUI : reg[10] = 0xfffff << 12
			wait for 5 ns;
			assert false report "11;0xfffff537;LUI : reg[10] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000034" report "progcounter error at step 22" severity error;
			wait for 5 ns;

		-- execute instruction 11
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 23" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 23" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 23" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 23" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 23" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 23" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 23" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 23" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 23" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 23" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 23" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 23" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 23" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 23" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 23" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 23" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 23" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 23" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 23" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 23" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 23" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 23" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 23" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 23" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 23" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 23" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 23" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 23" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 23" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 23" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 23" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 23" severity error;
			assert progcounter = x"00000038" report "progcounter error at step 23" severity error;
			wait for 5 ns;

		-- load instruction 12
			ck <= '0';
			instr <= x"fffff5b7"; -- LUI : reg[11] = 0xfffff << 12
			wait for 5 ns;
			assert false report "12;0xfffff5b7;LUI : reg[11] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000038" report "progcounter error at step 24" severity error;
			wait for 5 ns;

		-- execute instruction 12
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 25" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 25" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 25" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 25" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 25" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 25" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 25" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 25" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 25" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 25" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 25" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 25" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 25" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 25" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 25" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 25" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 25" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 25" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 25" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 25" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 25" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 25" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 25" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 25" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 25" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 25" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 25" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 25" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 25" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 25" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 25" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 25" severity error;
			assert progcounter = x"0000003c" report "progcounter error at step 25" severity error;
			wait for 5 ns;

		-- load instruction 13
			ck <= '0';
			instr <= x"fffff637"; -- LUI : reg[12] = 0xfffff << 12
			wait for 5 ns;
			assert false report "13;0xfffff637;LUI : reg[12] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000003c" report "progcounter error at step 26" severity error;
			wait for 5 ns;

		-- execute instruction 13
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 27" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 27" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 27" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 27" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 27" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 27" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 27" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 27" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 27" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 27" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 27" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 27" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 27" severity error;
			assert reg0d = x"00000000" report "reg0d error at step 27" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 27" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 27" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 27" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 27" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 27" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 27" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 27" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 27" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 27" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 27" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 27" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 27" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 27" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 27" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 27" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 27" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 27" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 27" severity error;
			assert progcounter = x"00000040" report "progcounter error at step 27" severity error;
			wait for 5 ns;

		-- load instruction 14
			ck <= '0';
			instr <= x"fffff6b7"; -- LUI : reg[13] = 0xfffff << 12
			wait for 5 ns;
			assert false report "14;0xfffff6b7;LUI : reg[13] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000040" report "progcounter error at step 28" severity error;
			wait for 5 ns;

		-- execute instruction 14
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 29" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 29" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 29" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 29" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 29" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 29" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 29" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 29" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 29" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 29" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 29" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 29" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 29" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 29" severity error;
			assert reg0e = x"00000000" report "reg0e error at step 29" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 29" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 29" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 29" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 29" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 29" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 29" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 29" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 29" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 29" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 29" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 29" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 29" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 29" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 29" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 29" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 29" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 29" severity error;
			assert progcounter = x"00000044" report "progcounter error at step 29" severity error;
			wait for 5 ns;

		-- load instruction 15
			ck <= '0';
			instr <= x"fffff737"; -- LUI : reg[14] = 0xfffff << 12
			wait for 5 ns;
			assert false report "15;0xfffff737;LUI : reg[14] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000044" report "progcounter error at step 30" severity error;
			wait for 5 ns;

		-- execute instruction 15
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 31" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 31" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 31" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 31" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 31" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 31" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 31" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 31" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 31" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 31" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 31" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 31" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 31" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 31" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 31" severity error;
			assert reg0f = x"00000000" report "reg0f error at step 31" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 31" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 31" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 31" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 31" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 31" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 31" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 31" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 31" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 31" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 31" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 31" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 31" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 31" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 31" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 31" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 31" severity error;
			assert progcounter = x"00000048" report "progcounter error at step 31" severity error;
			wait for 5 ns;

		-- load instruction 16
			ck <= '0';
			instr <= x"fffff7b7"; -- LUI : reg[15] = 0xfffff << 12
			wait for 5 ns;
			assert false report "16;0xfffff7b7;LUI : reg[15] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000048" report "progcounter error at step 32" severity error;
			wait for 5 ns;

		-- execute instruction 16
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 33" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 33" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 33" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 33" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 33" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 33" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 33" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 33" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 33" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 33" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 33" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 33" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 33" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 33" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 33" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 33" severity error;
			assert reg10 = x"00000000" report "reg10 error at step 33" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 33" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 33" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 33" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 33" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 33" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 33" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 33" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 33" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 33" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 33" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 33" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 33" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 33" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 33" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 33" severity error;
			assert progcounter = x"0000004c" report "progcounter error at step 33" severity error;
			wait for 5 ns;

		-- load instruction 17
			ck <= '0';
			instr <= x"fffff837"; -- LUI : reg[16] = 0xfffff << 12
			wait for 5 ns;
			assert false report "17;0xfffff837;LUI : reg[16] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000004c" report "progcounter error at step 34" severity error;
			wait for 5 ns;

		-- execute instruction 17
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 35" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 35" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 35" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 35" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 35" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 35" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 35" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 35" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 35" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 35" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 35" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 35" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 35" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 35" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 35" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 35" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 35" severity error;
			assert reg11 = x"00000000" report "reg11 error at step 35" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 35" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 35" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 35" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 35" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 35" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 35" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 35" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 35" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 35" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 35" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 35" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 35" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 35" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 35" severity error;
			assert progcounter = x"00000050" report "progcounter error at step 35" severity error;
			wait for 5 ns;

		-- load instruction 18
			ck <= '0';
			instr <= x"fffff8b7"; -- LUI : reg[17] = 0xfffff << 12
			wait for 5 ns;
			assert false report "18;0xfffff8b7;LUI : reg[17] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000050" report "progcounter error at step 36" severity error;
			wait for 5 ns;

		-- execute instruction 18
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 37" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 37" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 37" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 37" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 37" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 37" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 37" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 37" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 37" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 37" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 37" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 37" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 37" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 37" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 37" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 37" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 37" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 37" severity error;
			assert reg12 = x"00000000" report "reg12 error at step 37" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 37" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 37" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 37" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 37" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 37" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 37" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 37" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 37" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 37" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 37" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 37" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 37" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 37" severity error;
			assert progcounter = x"00000054" report "progcounter error at step 37" severity error;
			wait for 5 ns;

		-- load instruction 19
			ck <= '0';
			instr <= x"fffff937"; -- LUI : reg[18] = 0xfffff << 12
			wait for 5 ns;
			assert false report "19;0xfffff937;LUI : reg[18] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000054" report "progcounter error at step 38" severity error;
			wait for 5 ns;

		-- execute instruction 19
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 39" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 39" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 39" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 39" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 39" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 39" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 39" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 39" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 39" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 39" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 39" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 39" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 39" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 39" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 39" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 39" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 39" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 39" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 39" severity error;
			assert reg13 = x"00000000" report "reg13 error at step 39" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 39" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 39" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 39" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 39" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 39" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 39" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 39" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 39" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 39" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 39" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 39" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 39" severity error;
			assert progcounter = x"00000058" report "progcounter error at step 39" severity error;
			wait for 5 ns;

		-- load instruction 20
			ck <= '0';
			instr <= x"fffff9b7"; -- LUI : reg[19] = 0xfffff << 12
			wait for 5 ns;
			assert false report "20;0xfffff9b7;LUI : reg[19] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000058" report "progcounter error at step 40" severity error;
			wait for 5 ns;

		-- execute instruction 20
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 41" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 41" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 41" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 41" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 41" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 41" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 41" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 41" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 41" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 41" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 41" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 41" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 41" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 41" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 41" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 41" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 41" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 41" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 41" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 41" severity error;
			assert reg14 = x"00000000" report "reg14 error at step 41" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 41" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 41" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 41" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 41" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 41" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 41" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 41" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 41" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 41" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 41" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 41" severity error;
			assert progcounter = x"0000005c" report "progcounter error at step 41" severity error;
			wait for 5 ns;

		-- load instruction 21
			ck <= '0';
			instr <= x"fffffa37"; -- LUI : reg[20] = 0xfffff << 12
			wait for 5 ns;
			assert false report "21;0xfffffa37;LUI : reg[20] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000005c" report "progcounter error at step 42" severity error;
			wait for 5 ns;

		-- execute instruction 21
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 43" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 43" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 43" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 43" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 43" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 43" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 43" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 43" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 43" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 43" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 43" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 43" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 43" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 43" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 43" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 43" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 43" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 43" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 43" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 43" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 43" severity error;
			assert reg15 = x"00000000" report "reg15 error at step 43" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 43" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 43" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 43" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 43" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 43" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 43" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 43" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 43" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 43" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 43" severity error;
			assert progcounter = x"00000060" report "progcounter error at step 43" severity error;
			wait for 5 ns;

		-- load instruction 22
			ck <= '0';
			instr <= x"fffffab7"; -- LUI : reg[21] = 0xfffff << 12
			wait for 5 ns;
			assert false report "22;0xfffffab7;LUI : reg[21] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000060" report "progcounter error at step 44" severity error;
			wait for 5 ns;

		-- execute instruction 22
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 45" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 45" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 45" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 45" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 45" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 45" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 45" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 45" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 45" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 45" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 45" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 45" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 45" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 45" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 45" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 45" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 45" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 45" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 45" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 45" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 45" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 45" severity error;
			assert reg16 = x"00000000" report "reg16 error at step 45" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 45" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 45" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 45" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 45" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 45" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 45" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 45" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 45" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 45" severity error;
			assert progcounter = x"00000064" report "progcounter error at step 45" severity error;
			wait for 5 ns;

		-- load instruction 23
			ck <= '0';
			instr <= x"fffffb37"; -- LUI : reg[22] = 0xfffff << 12
			wait for 5 ns;
			assert false report "23;0xfffffb37;LUI : reg[22] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000064" report "progcounter error at step 46" severity error;
			wait for 5 ns;

		-- execute instruction 23
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 47" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 47" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 47" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 47" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 47" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 47" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 47" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 47" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 47" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 47" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 47" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 47" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 47" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 47" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 47" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 47" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 47" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 47" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 47" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 47" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 47" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 47" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 47" severity error;
			assert reg17 = x"00000000" report "reg17 error at step 47" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 47" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 47" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 47" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 47" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 47" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 47" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 47" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 47" severity error;
			assert progcounter = x"00000068" report "progcounter error at step 47" severity error;
			wait for 5 ns;

		-- load instruction 24
			ck <= '0';
			instr <= x"fffffbb7"; -- LUI : reg[23] = 0xfffff << 12
			wait for 5 ns;
			assert false report "24;0xfffffbb7;LUI : reg[23] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000068" report "progcounter error at step 48" severity error;
			wait for 5 ns;

		-- execute instruction 24
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 49" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 49" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 49" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 49" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 49" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 49" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 49" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 49" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 49" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 49" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 49" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 49" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 49" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 49" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 49" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 49" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 49" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 49" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 49" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 49" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 49" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 49" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 49" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 49" severity error;
			assert reg18 = x"00000000" report "reg18 error at step 49" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 49" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 49" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 49" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 49" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 49" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 49" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 49" severity error;
			assert progcounter = x"0000006c" report "progcounter error at step 49" severity error;
			wait for 5 ns;

		-- load instruction 25
			ck <= '0';
			instr <= x"fffffc37"; -- LUI : reg[24] = 0xfffff << 12
			wait for 5 ns;
			assert false report "25;0xfffffc37;LUI : reg[24] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000006c" report "progcounter error at step 50" severity error;
			wait for 5 ns;

		-- execute instruction 25
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 51" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 51" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 51" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 51" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 51" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 51" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 51" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 51" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 51" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 51" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 51" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 51" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 51" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 51" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 51" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 51" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 51" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 51" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 51" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 51" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 51" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 51" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 51" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 51" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 51" severity error;
			assert reg19 = x"00000000" report "reg19 error at step 51" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 51" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 51" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 51" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 51" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 51" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 51" severity error;
			assert progcounter = x"00000070" report "progcounter error at step 51" severity error;
			wait for 5 ns;

		-- load instruction 26
			ck <= '0';
			instr <= x"fffffcb7"; -- LUI : reg[25] = 0xfffff << 12
			wait for 5 ns;
			assert false report "26;0xfffffcb7;LUI : reg[25] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000070" report "progcounter error at step 52" severity error;
			wait for 5 ns;

		-- execute instruction 26
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 53" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 53" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 53" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 53" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 53" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 53" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 53" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 53" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 53" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 53" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 53" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 53" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 53" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 53" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 53" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 53" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 53" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 53" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 53" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 53" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 53" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 53" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 53" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 53" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 53" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 53" severity error;
			assert reg1a = x"00000000" report "reg1a error at step 53" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 53" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 53" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 53" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 53" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 53" severity error;
			assert progcounter = x"00000074" report "progcounter error at step 53" severity error;
			wait for 5 ns;

		-- load instruction 27
			ck <= '0';
			instr <= x"fffffd37"; -- LUI : reg[26] = 0xfffff << 12
			wait for 5 ns;
			assert false report "27;0xfffffd37;LUI : reg[26] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000074" report "progcounter error at step 54" severity error;
			wait for 5 ns;

		-- execute instruction 27
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 55" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 55" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 55" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 55" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 55" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 55" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 55" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 55" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 55" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 55" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 55" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 55" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 55" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 55" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 55" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 55" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 55" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 55" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 55" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 55" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 55" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 55" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 55" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 55" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 55" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 55" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 55" severity error;
			assert reg1b = x"00000000" report "reg1b error at step 55" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 55" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 55" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 55" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 55" severity error;
			assert progcounter = x"00000078" report "progcounter error at step 55" severity error;
			wait for 5 ns;

		-- load instruction 28
			ck <= '0';
			instr <= x"fffffdb7"; -- LUI : reg[27] = 0xfffff << 12
			wait for 5 ns;
			assert false report "28;0xfffffdb7;LUI : reg[27] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000078" report "progcounter error at step 56" severity error;
			wait for 5 ns;

		-- execute instruction 28
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 57" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 57" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 57" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 57" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 57" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 57" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 57" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 57" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 57" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 57" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 57" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 57" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 57" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 57" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 57" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 57" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 57" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 57" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 57" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 57" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 57" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 57" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 57" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 57" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 57" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 57" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 57" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 57" severity error;
			assert reg1c = x"00000000" report "reg1c error at step 57" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 57" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 57" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 57" severity error;
			assert progcounter = x"0000007c" report "progcounter error at step 57" severity error;
			wait for 5 ns;

		-- load instruction 29
			ck <= '0';
			instr <= x"fffffe37"; -- LUI : reg[28] = 0xfffff << 12
			wait for 5 ns;
			assert false report "29;0xfffffe37;LUI : reg[28] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000007c" report "progcounter error at step 58" severity error;
			wait for 5 ns;

		-- execute instruction 29
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 59" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 59" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 59" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 59" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 59" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 59" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 59" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 59" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 59" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 59" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 59" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 59" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 59" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 59" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 59" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 59" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 59" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 59" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 59" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 59" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 59" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 59" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 59" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 59" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 59" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 59" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 59" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 59" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 59" severity error;
			assert reg1d = x"00000000" report "reg1d error at step 59" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 59" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 59" severity error;
			assert progcounter = x"00000080" report "progcounter error at step 59" severity error;
			wait for 5 ns;

		-- load instruction 30
			ck <= '0';
			instr <= x"fffffeb7"; -- LUI : reg[29] = 0xfffff << 12
			wait for 5 ns;
			assert false report "30;0xfffffeb7;LUI : reg[29] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000080" report "progcounter error at step 60" severity error;
			wait for 5 ns;

		-- execute instruction 30
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 61" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 61" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 61" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 61" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 61" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 61" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 61" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 61" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 61" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 61" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 61" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 61" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 61" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 61" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 61" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 61" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 61" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 61" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 61" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 61" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 61" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 61" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 61" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 61" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 61" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 61" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 61" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 61" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 61" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 61" severity error;
			assert reg1e = x"00000000" report "reg1e error at step 61" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 61" severity error;
			assert progcounter = x"00000084" report "progcounter error at step 61" severity error;
			wait for 5 ns;

		-- load instruction 31
			ck <= '0';
			instr <= x"ffffff37"; -- LUI : reg[30] = 0xfffff << 12
			wait for 5 ns;
			assert false report "31;0xffffff37;LUI : reg[30] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000084" report "progcounter error at step 62" severity error;
			wait for 5 ns;

		-- execute instruction 31
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 63" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 63" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 63" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 63" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 63" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 63" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 63" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 63" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 63" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 63" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 63" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 63" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 63" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 63" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 63" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 63" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 63" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 63" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 63" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 63" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 63" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 63" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 63" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 63" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 63" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 63" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 63" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 63" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 63" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 63" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 63" severity error;
			assert reg1f = x"00000000" report "reg1f error at step 63" severity error;
			assert progcounter = x"00000088" report "progcounter error at step 63" severity error;
			wait for 5 ns;

		-- load instruction 32
			ck <= '0';
			instr <= x"ffffffb7"; -- LUI : reg[31] = 0xfffff << 12
			wait for 5 ns;
			assert false report "32;0xffffffb7;LUI : reg[31] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000088" report "progcounter error at step 64" severity error;
			wait for 5 ns;

		-- execute instruction 32
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 65" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 65" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 65" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 65" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 65" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 65" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 65" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 65" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 65" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 65" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 65" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 65" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 65" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 65" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 65" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 65" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 65" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 65" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 65" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 65" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 65" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 65" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 65" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 65" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 65" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 65" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 65" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 65" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 65" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 65" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 65" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 65" severity error;
			assert progcounter = x"0000008c" report "progcounter error at step 65" severity error;
			wait for 5 ns;

		-- load instruction 33
			ck <= '0';
			instr <= x"ffffffb7"; -- LUI : reg[31] = 0xfffff << 12
			wait for 5 ns;
			assert false report "33;0xffffffb7;LUI : reg[31] = 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"0000008c" report "progcounter error at step 66" severity error;
			wait for 5 ns;

		-- execute instruction 33
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 67" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 67" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 67" severity error;
			assert reg03 = x"0ffff000" report "reg03 error at step 67" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 67" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 67" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 67" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 67" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 67" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 67" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 67" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 67" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 67" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 67" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 67" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 67" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 67" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 67" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 67" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 67" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 67" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 67" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 67" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 67" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 67" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 67" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 67" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 67" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 67" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 67" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 67" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 67" severity error;
			assert progcounter = x"00000090" report "progcounter error at step 67" severity error;
			wait for 5 ns;

		-- load instruction 34
			ck <= '0';
			instr <= x"fffff197"; -- AUIPC : reg[03] = PC + 0xfffff << 12
			wait for 5 ns;
			assert false report "34;0xfffff197;AUIPC : reg[03] = PC + 0xfffff << 12;OK; ;" severity note;
			assert progcounter = x"00000090" report "progcounter error at step 68" severity error;
			wait for 5 ns;

		-- execute instruction 34
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 69" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 69" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 69" severity error;
			assert reg03 = x"fffff090" report "reg03 error at step 69" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 69" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 69" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 69" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 69" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 69" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 69" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 69" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 69" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 69" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 69" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 69" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 69" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 69" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 69" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 69" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 69" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 69" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 69" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 69" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 69" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 69" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 69" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 69" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 69" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 69" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 69" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 69" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 69" severity error;
			assert progcounter = x"00000094" report "progcounter error at step 69" severity error;
			wait for 5 ns;

		-- load instruction 35
			ck <= '0';
			instr <= x"00000197"; -- AUIPC : reg[03] = PC + 0x0 << 12
			wait for 5 ns;
			assert false report "35;0x00000197;AUIPC : reg[03] = PC + 0x0 << 12;OK; ;" severity note;
			assert progcounter = x"00000094" report "progcounter error at step 70" severity error;
			wait for 5 ns;

		-- execute instruction 35
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 71" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 71" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 71" severity error;
			assert reg03 = x"00000094" report "reg03 error at step 71" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 71" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 71" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 71" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 71" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 71" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 71" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 71" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 71" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 71" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 71" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 71" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 71" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 71" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 71" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 71" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 71" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 71" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 71" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 71" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 71" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 71" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 71" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 71" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 71" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 71" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 71" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 71" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 71" severity error;
			assert progcounter = x"00000098" report "progcounter error at step 71" severity error;
			wait for 5 ns;

		-- load instruction 36
			ck <= '0';
			instr <= x"00004197"; -- AUIPC : reg[03] = PC + 0x4 << 12
			wait for 5 ns;
			assert false report "36;0x00004197;AUIPC : reg[03] = PC + 0x4 << 12;OK; ;" severity note;
			assert progcounter = x"00000098" report "progcounter error at step 72" severity error;
			wait for 5 ns;

		-- execute instruction 36
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 73" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 73" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 73" severity error;
			assert reg03 = x"00004098" report "reg03 error at step 73" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 73" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 73" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 73" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 73" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 73" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 73" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 73" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 73" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 73" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 73" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 73" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 73" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 73" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 73" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 73" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 73" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 73" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 73" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 73" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 73" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 73" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 73" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 73" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 73" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 73" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 73" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 73" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 73" severity error;
			assert progcounter = x"0000009c" report "progcounter error at step 73" severity error;
			wait for 5 ns;

		-- load instruction 37
			ck <= '0';
			instr <= x"0080006f"; -- JAL : reg[00] = PC+4 and PC = 0x9c + 8
			wait for 5 ns;
			assert false report "37;0x0080006f;JAL : reg[00] = PC+4 and PC = 0x9c + 8;OK; ;" severity note;
			assert progcounter = x"0000009c" report "progcounter error at step 74" severity error;
			wait for 5 ns;

		-- execute instruction 37
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 75" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 75" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 75" severity error;
			assert reg03 = x"00004098" report "reg03 error at step 75" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 75" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 75" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 75" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 75" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 75" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 75" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 75" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 75" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 75" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 75" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 75" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 75" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 75" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 75" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 75" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 75" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 75" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 75" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 75" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 75" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 75" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 75" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 75" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 75" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 75" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 75" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 75" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 75" severity error;
			assert progcounter = x"000000a4" report "progcounter error at step 75" severity error;
			wait for 5 ns;

		-- load instruction 38
			ck <= '0';
			instr <= x"ffdff06f"; -- JAL : reg[00] = PC+4 and PC = 0xa4 + -4
			wait for 5 ns;
			assert false report "38;0xffdff06f;JAL : reg[00] = PC+4 and PC = 0xa4 + -4;OK; ;" severity note;
			assert progcounter = x"000000a4" report "progcounter error at step 76" severity error;
			wait for 5 ns;

		-- execute instruction 38
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 77" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 77" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 77" severity error;
			assert reg03 = x"00004098" report "reg03 error at step 77" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 77" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 77" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 77" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 77" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 77" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 77" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 77" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 77" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 77" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 77" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 77" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 77" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 77" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 77" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 77" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 77" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 77" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 77" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 77" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 77" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 77" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 77" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 77" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 77" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 77" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 77" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 77" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 77" severity error;
			assert progcounter = x"000000a0" report "progcounter error at step 77" severity error;
			wait for 5 ns;

		-- load instruction 39
			ck <= '0';
			instr <= x"0080006f"; -- JAL : reg[00] = PC+4 and PC = 0xa0 + 8
			wait for 5 ns;
			assert false report "39;0x0080006f;JAL : reg[00] = PC+4 and PC = 0xa0 + 8;OK; ;" severity note;
			assert progcounter = x"000000a0" report "progcounter error at step 78" severity error;
			wait for 5 ns;

		-- execute instruction 39
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 79" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 79" severity error;
			assert reg02 = x"000ff000" report "reg02 error at step 79" severity error;
			assert reg03 = x"00004098" report "reg03 error at step 79" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 79" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 79" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 79" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 79" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 79" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 79" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 79" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 79" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 79" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 79" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 79" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 79" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 79" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 79" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 79" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 79" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 79" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 79" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 79" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 79" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 79" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 79" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 79" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 79" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 79" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 79" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 79" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 79" severity error;
			assert progcounter = x"000000a8" report "progcounter error at step 79" severity error;
			wait for 5 ns;

		-- load instruction 40
			ck <= '0';
			instr <= x"0040016f"; -- JAL : reg[02] = PC+4 and PC = 0xa8 + 4
			wait for 5 ns;
			assert false report "40;0x0040016f;JAL : reg[02] = PC+4 and PC = 0xa8 + 4;OK; ;" severity note;
			assert progcounter = x"000000a8" report "progcounter error at step 80" severity error;
			wait for 5 ns;

		-- execute instruction 40
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 81" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 81" severity error;
			assert reg02 = x"000000ac" report "reg02 error at step 81" severity error;
			assert reg03 = x"00004098" report "reg03 error at step 81" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 81" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 81" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 81" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 81" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 81" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 81" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 81" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 81" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 81" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 81" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 81" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 81" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 81" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 81" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 81" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 81" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 81" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 81" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 81" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 81" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 81" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 81" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 81" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 81" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 81" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 81" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 81" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 81" severity error;
			assert progcounter = x"000000ac" report "progcounter error at step 81" severity error;
			wait for 5 ns;

		-- load instruction 41
			ck <= '0';
			instr <= x"005101e7"; -- JALR : reg[03] = PC+4 and PC = (reg[02] + 5) & ~1 
			wait for 5 ns;
			assert false report "41;0x005101e7;JALR : reg[03] = PC+4 and PC = (reg[02] + 5) & ~1;OK; ;" severity note;
			assert progcounter = x"000000ac" report "progcounter error at step 82" severity error;
			wait for 5 ns;

		-- execute instruction 41
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 83" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 83" severity error;
			assert reg02 = x"000000ac" report "reg02 error at step 83" severity error;
			assert reg03 = x"000000b0" report "reg03 error at step 83" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 83" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 83" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 83" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 83" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 83" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 83" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 83" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 83" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 83" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 83" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 83" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 83" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 83" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 83" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 83" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 83" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 83" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 83" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 83" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 83" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 83" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 83" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 83" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 83" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 83" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 83" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 83" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 83" severity error;
			assert progcounter = x"000000b0" report "progcounter error at step 83" severity error;
			wait for 5 ns;

		-- load instruction 42
			ck <= '0';
			instr <= x"00000197"; -- AUIPC : reg[03] = PC + 0x0 << 12
			wait for 5 ns;
			assert false report "42;0x00000197;AUIPC : reg[03] = PC + 0x0 << 12;OK; ;" severity note;
			assert progcounter = x"000000b0" report "progcounter error at step 84" severity error;
			wait for 5 ns;

		-- execute instruction 42
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 85" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 85" severity error;
			assert reg02 = x"000000ac" report "reg02 error at step 85" severity error;
			assert reg03 = x"000000b0" report "reg03 error at step 85" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 85" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 85" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 85" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 85" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 85" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 85" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 85" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 85" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 85" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 85" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 85" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 85" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 85" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 85" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 85" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 85" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 85" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 85" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 85" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 85" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 85" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 85" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 85" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 85" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 85" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 85" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 85" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 85" severity error;
			assert progcounter = x"000000b4" report "progcounter error at step 85" severity error;
			wait for 5 ns;

		-- load instruction 43
			ck <= '0';
			instr <= x"00818193"; -- ADDI : reg[03] = reg[03] + 8
			wait for 5 ns;
			assert false report "43;0x00818193;ADDI : reg[03] = reg[03] + 8;OK; ;" severity note;
			assert progcounter = x"000000b4" report "progcounter error at step 86" severity error;
			wait for 5 ns;

		-- execute instruction 43
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 87" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 87" severity error;
			assert reg02 = x"000000ac" report "reg02 error at step 87" severity error;
			assert reg03 = x"000000b8" report "reg03 error at step 87" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 87" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 87" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 87" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 87" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 87" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 87" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 87" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 87" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 87" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 87" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 87" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 87" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 87" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 87" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 87" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 87" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 87" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 87" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 87" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 87" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 87" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 87" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 87" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 87" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 87" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 87" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 87" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 87" severity error;
			assert progcounter = x"000000b8" report "progcounter error at step 87" severity error;
			wait for 5 ns;

		-- load instruction 44
			ck <= '0';
			instr <= x"ffc18167"; -- JALR : reg[02] = PC+4 and PC = (reg[03] + -4) & ~1 
			wait for 5 ns;
			assert false report "44;0xffc18167;JALR : reg[02] = PC+4 and PC = (reg[03] + -4) & ~1;OK; ;" severity note;
			assert progcounter = x"000000b8" report "progcounter error at step 88" severity error;
			wait for 5 ns;

		-- execute instruction 44
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 89" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 89" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 89" severity error;
			assert reg03 = x"000000b8" report "reg03 error at step 89" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 89" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 89" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 89" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 89" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 89" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 89" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 89" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 89" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 89" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 89" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 89" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 89" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 89" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 89" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 89" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 89" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 89" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 89" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 89" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 89" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 89" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 89" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 89" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 89" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 89" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 89" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 89" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 89" severity error;
			assert progcounter = x"000000b4" report "progcounter error at step 89" severity error;
			wait for 5 ns;

		-- load instruction 45
			ck <= '0';
			instr <= x"00818193"; -- ADDI : reg[03] = reg[03] + 8
			wait for 5 ns;
			assert false report "45;0x00818193;ADDI : reg[03] = reg[03] + 8;OK; ;" severity note;
			assert progcounter = x"000000b4" report "progcounter error at step 90" severity error;
			wait for 5 ns;

		-- execute instruction 45
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 91" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 91" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 91" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 91" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 91" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 91" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 91" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 91" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 91" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 91" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 91" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 91" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 91" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 91" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 91" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 91" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 91" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 91" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 91" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 91" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 91" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 91" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 91" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 91" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 91" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 91" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 91" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 91" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 91" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 91" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 91" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 91" severity error;
			assert progcounter = x"000000b8" report "progcounter error at step 91" severity error;
			wait for 5 ns;

		-- load instruction 46
			ck <= '0';
			instr <= x"ffc18167"; -- JALR : reg[02] = PC+4 and PC = (reg[03] + -4) & ~1 
			wait for 5 ns;
			assert false report "46;0xffc18167;JALR : reg[02] = PC+4 and PC = (reg[03] + -4) & ~1;OK; ;" severity note;
			assert progcounter = x"000000b8" report "progcounter error at step 92" severity error;
			wait for 5 ns;

		-- execute instruction 46
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 93" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 93" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 93" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 93" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 93" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 93" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 93" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 93" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 93" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 93" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 93" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 93" severity error;
			assert reg0c = x"fffff000" report "reg0c error at step 93" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 93" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 93" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 93" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 93" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 93" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 93" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 93" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 93" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 93" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 93" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 93" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 93" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 93" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 93" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 93" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 93" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 93" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 93" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 93" severity error;
			assert progcounter = x"000000bc" report "progcounter error at step 93" severity error;
			wait for 5 ns;

		-- load instruction 47
			ck <= '0';
			instr <= x"00300613"; -- ADDI : reg[12] = reg[00] + 3
			wait for 5 ns;
			assert false report "47;0x00300613;ADDI : reg[12] = reg[00] + 3;OK; ;" severity note;
			assert progcounter = x"000000bc" report "progcounter error at step 94" severity error;
			wait for 5 ns;

		-- execute instruction 47
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 95" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 95" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 95" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 95" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 95" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 95" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 95" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 95" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 95" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 95" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 95" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 95" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 95" severity error;
			assert reg0d = x"fffff000" report "reg0d error at step 95" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 95" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 95" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 95" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 95" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 95" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 95" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 95" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 95" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 95" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 95" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 95" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 95" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 95" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 95" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 95" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 95" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 95" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 95" severity error;
			assert progcounter = x"000000c0" report "progcounter error at step 95" severity error;
			wait for 5 ns;

		-- load instruction 48
			ck <= '0';
			instr <= x"00400693"; -- ADDI : reg[13] = reg[00] + 4
			wait for 5 ns;
			assert false report "48;0x00400693;ADDI : reg[13] = reg[00] + 4;OK; ;" severity note;
			assert progcounter = x"000000c0" report "progcounter error at step 96" severity error;
			wait for 5 ns;

		-- execute instruction 48
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 97" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 97" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 97" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 97" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 97" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 97" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 97" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 97" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 97" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 97" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 97" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 97" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 97" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 97" severity error;
			assert reg0e = x"fffff000" report "reg0e error at step 97" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 97" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 97" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 97" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 97" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 97" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 97" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 97" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 97" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 97" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 97" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 97" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 97" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 97" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 97" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 97" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 97" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 97" severity error;
			assert progcounter = x"000000c4" report "progcounter error at step 97" severity error;
			wait for 5 ns;

		-- load instruction 49
			ck <= '0';
			instr <= x"00400713"; -- ADDI : reg[14] = reg[00] + 4
			wait for 5 ns;
			assert false report "49;0x00400713;ADDI : reg[14] = reg[00] + 4;OK; ;" severity note;
			assert progcounter = x"000000c4" report "progcounter error at step 98" severity error;
			wait for 5 ns;

		-- execute instruction 49
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 99" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 99" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 99" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 99" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 99" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 99" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 99" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 99" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 99" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 99" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 99" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 99" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 99" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 99" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 99" severity error;
			assert reg0f = x"fffff000" report "reg0f error at step 99" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 99" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 99" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 99" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 99" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 99" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 99" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 99" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 99" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 99" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 99" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 99" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 99" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 99" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 99" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 99" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 99" severity error;
			assert progcounter = x"000000c8" report "progcounter error at step 99" severity error;
			wait for 5 ns;

		-- load instruction 50
			ck <= '0';
			instr <= x"ffb00793"; -- ADDI : reg[15] = reg[00] + -5
			wait for 5 ns;
			assert false report "50;0xffb00793;ADDI : reg[15] = reg[00] + -5;OK; ;" severity note;
			assert progcounter = x"000000c8" report "progcounter error at step 100" severity error;
			wait for 5 ns;

		-- execute instruction 50
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 101" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 101" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 101" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 101" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 101" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 101" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 101" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 101" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 101" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 101" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 101" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 101" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 101" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 101" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 101" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 101" severity error;
			assert reg10 = x"fffff000" report "reg10 error at step 101" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 101" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 101" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 101" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 101" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 101" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 101" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 101" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 101" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 101" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 101" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 101" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 101" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 101" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 101" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 101" severity error;
			assert progcounter = x"000000cc" report "progcounter error at step 101" severity error;
			wait for 5 ns;

		-- load instruction 51
			ck <= '0';
			instr <= x"ffc00813"; -- ADDI : reg[16] = reg[00] + -4
			wait for 5 ns;
			assert false report "51;0xffc00813;ADDI : reg[16] = reg[00] + -4;OK; ;" severity note;
			assert progcounter = x"000000cc" report "progcounter error at step 102" severity error;
			wait for 5 ns;

		-- execute instruction 51
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 103" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 103" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 103" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 103" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 103" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 103" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 103" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 103" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 103" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 103" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 103" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 103" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 103" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 103" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 103" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 103" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 103" severity error;
			assert reg11 = x"fffff000" report "reg11 error at step 103" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 103" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 103" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 103" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 103" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 103" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 103" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 103" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 103" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 103" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 103" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 103" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 103" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 103" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 103" severity error;
			assert progcounter = x"000000d0" report "progcounter error at step 103" severity error;
			wait for 5 ns;

		-- load instruction 52
			ck <= '0';
			instr <= x"ffc00893"; -- ADDI : reg[17] = reg[00] + -4
			wait for 5 ns;
			assert false report "52;0xffc00893;ADDI : reg[17] = reg[00] + -4;OK; ;" severity note;
			assert progcounter = x"000000d0" report "progcounter error at step 104" severity error;
			wait for 5 ns;

		-- execute instruction 52
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 105" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 105" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 105" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 105" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 105" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 105" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 105" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 105" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 105" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 105" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 105" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 105" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 105" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 105" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 105" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 105" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 105" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 105" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 105" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 105" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 105" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 105" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 105" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 105" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 105" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 105" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 105" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 105" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 105" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 105" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 105" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 105" severity error;
			assert progcounter = x"000000d4" report "progcounter error at step 105" severity error;
			wait for 5 ns;

		-- load instruction 53
			ck <= '0';
			instr <= x"01c0006f"; -- JAL : reg[00] = PC+4 and PC = 0xd4 + 28
			wait for 5 ns;
			assert false report "53;0x01c0006f;JAL : reg[00] = PC+4 and PC = 0xd4 + 28;OK; ;" severity note;
			assert progcounter = x"000000d4" report "progcounter error at step 106" severity error;
			wait for 5 ns;

		-- execute instruction 53
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 107" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 107" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 107" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 107" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 107" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 107" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 107" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 107" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 107" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 107" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 107" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 107" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 107" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 107" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 107" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 107" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 107" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 107" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 107" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 107" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 107" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 107" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 107" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 107" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 107" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 107" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 107" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 107" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 107" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 107" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 107" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 107" severity error;
			assert progcounter = x"000000f0" report "progcounter error at step 107" severity error;
			wait for 5 ns;

		-- load instruction 54
			ck <= '0';
			instr <= x"28d60663"; -- BEQ : if ( reg[12] == reg[13] ) PC = PC + 652
			wait for 5 ns;
			assert false report "54;0x28d60663;BEQ : if ( reg[12] == reg[13] ) PC = PC + 652;OK; ;" severity note;
			assert progcounter = x"000000f0" report "progcounter error at step 108" severity error;
			wait for 5 ns;

		-- execute instruction 54
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 109" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 109" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 109" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 109" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 109" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 109" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 109" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 109" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 109" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 109" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 109" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 109" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 109" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 109" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 109" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 109" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 109" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 109" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 109" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 109" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 109" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 109" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 109" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 109" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 109" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 109" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 109" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 109" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 109" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 109" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 109" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 109" severity error;
			assert progcounter = x"000000f4" report "progcounter error at step 109" severity error;
			wait for 5 ns;

		-- load instruction 55
			ck <= '0';
			instr <= x"28f60463"; -- BEQ : if ( reg[12] == reg[15] ) PC = PC + 648
			wait for 5 ns;
			assert false report "55;0x28f60463;BEQ : if ( reg[12] == reg[15] ) PC = PC + 648;OK; ;" severity note;
			assert progcounter = x"000000f4" report "progcounter error at step 110" severity error;
			wait for 5 ns;

		-- execute instruction 55
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 111" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 111" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 111" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 111" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 111" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 111" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 111" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 111" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 111" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 111" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 111" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 111" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 111" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 111" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 111" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 111" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 111" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 111" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 111" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 111" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 111" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 111" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 111" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 111" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 111" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 111" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 111" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 111" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 111" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 111" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 111" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 111" severity error;
			assert progcounter = x"000000f8" report "progcounter error at step 111" severity error;
			wait for 5 ns;

		-- load instruction 56
			ck <= '0';
			instr <= x"29078263"; -- BEQ : if ( reg[15] == reg[16] ) PC = PC + 644
			wait for 5 ns;
			assert false report "56;0x29078263;BEQ : if ( reg[15] == reg[16] ) PC = PC + 644;OK; ;" severity note;
			assert progcounter = x"000000f8" report "progcounter error at step 112" severity error;
			wait for 5 ns;

		-- execute instruction 56
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 113" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 113" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 113" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 113" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 113" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 113" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 113" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 113" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 113" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 113" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 113" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 113" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 113" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 113" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 113" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 113" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 113" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 113" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 113" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 113" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 113" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 113" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 113" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 113" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 113" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 113" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 113" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 113" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 113" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 113" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 113" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 113" severity error;
			assert progcounter = x"000000fc" report "progcounter error at step 113" severity error;
			wait for 5 ns;

		-- load instruction 57
			ck <= '0';
			instr <= x"00e68463"; -- BEQ : if ( reg[13] == reg[14] ) PC = PC + 8
			wait for 5 ns;
			assert false report "57;0x00e68463;BEQ : if ( reg[13] == reg[14] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"000000fc" report "progcounter error at step 114" severity error;
			wait for 5 ns;

		-- execute instruction 57
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 115" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 115" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 115" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 115" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 115" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 115" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 115" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 115" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 115" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 115" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 115" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 115" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 115" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 115" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 115" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 115" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 115" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 115" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 115" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 115" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 115" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 115" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 115" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 115" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 115" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 115" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 115" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 115" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 115" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 115" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 115" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 115" severity error;
			assert progcounter = x"00000104" report "progcounter error at step 115" severity error;
			wait for 5 ns;

		-- load instruction 58
			ck <= '0';
			instr <= x"fd180ae3"; -- BEQ : if ( reg[16] == reg[17] ) PC = PC + -44
			wait for 5 ns;
			assert false report "58;0xfd180ae3;BEQ : if ( reg[16] == reg[17] ) PC = PC + -44;OK; ;" severity note;
			assert progcounter = x"00000104" report "progcounter error at step 116" severity error;
			wait for 5 ns;

		-- execute instruction 58
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 117" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 117" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 117" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 117" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 117" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 117" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 117" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 117" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 117" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 117" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 117" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 117" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 117" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 117" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 117" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 117" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 117" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 117" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 117" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 117" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 117" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 117" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 117" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 117" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 117" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 117" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 117" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 117" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 117" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 117" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 117" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 117" severity error;
			assert progcounter = x"000000d8" report "progcounter error at step 117" severity error;
			wait for 5 ns;

		-- load instruction 59
			ck <= '0';
			instr <= x"0340006f"; -- JAL : reg[00] = PC+4 and PC = 0xd8 + 52
			wait for 5 ns;
			assert false report "59;0x0340006f;JAL : reg[00] = PC+4 and PC = 0xd8 + 52;OK; ;" severity note;
			assert progcounter = x"000000d8" report "progcounter error at step 118" severity error;
			wait for 5 ns;

		-- execute instruction 59
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 119" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 119" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 119" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 119" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 119" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 119" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 119" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 119" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 119" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 119" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 119" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 119" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 119" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 119" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 119" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 119" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 119" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 119" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 119" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 119" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 119" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 119" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 119" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 119" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 119" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 119" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 119" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 119" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 119" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 119" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 119" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 119" severity error;
			assert progcounter = x"0000010c" report "progcounter error at step 119" severity error;
			wait for 5 ns;

		-- load instruction 60
			ck <= '0';
			instr <= x"26e69863"; -- BNE : if ( reg[13] != reg[14] ) PC = PC + 624
			wait for 5 ns;
			assert false report "60;0x26e69863;BNE : if ( reg[13] != reg[14] ) PC = PC + 624;OK; ;" severity note;
			assert progcounter = x"0000010c" report "progcounter error at step 120" severity error;
			wait for 5 ns;

		-- execute instruction 60
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 121" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 121" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 121" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 121" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 121" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 121" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 121" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 121" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 121" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 121" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 121" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 121" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 121" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 121" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 121" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 121" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 121" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 121" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 121" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 121" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 121" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 121" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 121" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 121" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 121" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 121" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 121" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 121" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 121" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 121" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 121" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 121" severity error;
			assert progcounter = x"00000110" report "progcounter error at step 121" severity error;
			wait for 5 ns;

		-- load instruction 61
			ck <= '0';
			instr <= x"27181663"; -- BNE : if ( reg[16] != reg[17] ) PC = PC + 620
			wait for 5 ns;
			assert false report "61;0x27181663;BNE : if ( reg[16] != reg[17] ) PC = PC + 620;OK; ;" severity note;
			assert progcounter = x"00000110" report "progcounter error at step 122" severity error;
			wait for 5 ns;

		-- execute instruction 61
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 123" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 123" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 123" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 123" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 123" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 123" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 123" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 123" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 123" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 123" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 123" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 123" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 123" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 123" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 123" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 123" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 123" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 123" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 123" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 123" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 123" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 123" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 123" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 123" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 123" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 123" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 123" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 123" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 123" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 123" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 123" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 123" severity error;
			assert progcounter = x"00000114" report "progcounter error at step 123" severity error;
			wait for 5 ns;

		-- load instruction 62
			ck <= '0';
			instr <= x"00d61463"; -- BNE : if ( reg[12] != reg[13] ) PC = PC + 8
			wait for 5 ns;
			assert false report "62;0x00d61463;BNE : if ( reg[12] != reg[13] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"00000114" report "progcounter error at step 124" severity error;
			wait for 5 ns;

		-- execute instruction 62
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 125" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 125" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 125" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 125" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 125" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 125" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 125" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 125" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 125" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 125" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 125" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 125" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 125" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 125" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 125" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 125" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 125" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 125" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 125" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 125" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 125" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 125" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 125" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 125" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 125" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 125" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 125" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 125" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 125" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 125" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 125" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 125" severity error;
			assert progcounter = x"0000011c" report "progcounter error at step 125" severity error;
			wait for 5 ns;

		-- load instruction 63
			ck <= '0';
			instr <= x"00f61463"; -- BNE : if ( reg[12] != reg[15] ) PC = PC + 8
			wait for 5 ns;
			assert false report "63;0x00f61463;BNE : if ( reg[12] != reg[15] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"0000011c" report "progcounter error at step 126" severity error;
			wait for 5 ns;

		-- execute instruction 63
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 127" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 127" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 127" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 127" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 127" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 127" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 127" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 127" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 127" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 127" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 127" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 127" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 127" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 127" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 127" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 127" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 127" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 127" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 127" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 127" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 127" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 127" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 127" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 127" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 127" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 127" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 127" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 127" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 127" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 127" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 127" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 127" severity error;
			assert progcounter = x"00000124" report "progcounter error at step 127" severity error;
			wait for 5 ns;

		-- load instruction 64
			ck <= '0';
			instr <= x"fb079ce3"; -- BNE : if ( reg[15] != reg[16] ) PC = PC + -72
			wait for 5 ns;
			assert false report "64;0xfb079ce3;BNE : if ( reg[15] != reg[16] ) PC = PC + -72;OK; ;" severity note;
			assert progcounter = x"00000124" report "progcounter error at step 128" severity error;
			wait for 5 ns;

		-- execute instruction 64
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 129" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 129" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 129" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 129" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 129" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 129" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 129" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 129" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 129" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 129" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 129" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 129" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 129" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 129" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 129" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 129" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 129" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 129" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 129" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 129" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 129" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 129" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 129" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 129" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 129" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 129" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 129" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 129" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 129" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 129" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 129" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 129" severity error;
			assert progcounter = x"000000dc" report "progcounter error at step 129" severity error;
			wait for 5 ns;

		-- load instruction 65
			ck <= '0';
			instr <= x"0500006f"; -- JAL : reg[00] = PC+4 and PC = 0xdc + 80
			wait for 5 ns;
			assert false report "65;0x0500006f;JAL : reg[00] = PC+4 and PC = 0xdc + 80;OK; ;" severity note;
			assert progcounter = x"000000dc" report "progcounter error at step 130" severity error;
			wait for 5 ns;

		-- execute instruction 65
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 131" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 131" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 131" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 131" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 131" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 131" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 131" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 131" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 131" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 131" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 131" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 131" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 131" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 131" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 131" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 131" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 131" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 131" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 131" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 131" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 131" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 131" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 131" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 131" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 131" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 131" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 131" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 131" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 131" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 131" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 131" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 131" severity error;
			assert progcounter = x"0000012c" report "progcounter error at step 131" severity error;
			wait for 5 ns;

		-- load instruction 66
			ck <= '0';
			instr <= x"24d6c863"; -- BLT : if ( reg[13] < reg[13] ) PC = PC + 592
			wait for 5 ns;
			assert false report "66;0x24d6c863;BLT : if ( reg[13] < reg[13] ) PC = PC + 592;OK; ;" severity note;
			assert progcounter = x"0000012c" report "progcounter error at step 132" severity error;
			wait for 5 ns;

		-- execute instruction 66
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 133" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 133" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 133" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 133" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 133" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 133" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 133" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 133" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 133" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 133" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 133" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 133" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 133" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 133" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 133" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 133" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 133" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 133" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 133" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 133" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 133" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 133" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 133" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 133" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 133" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 133" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 133" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 133" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 133" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 133" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 133" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 133" severity error;
			assert progcounter = x"00000130" report "progcounter error at step 133" severity error;
			wait for 5 ns;

		-- load instruction 67
			ck <= '0';
			instr <= x"24f84663"; -- BLT : if ( reg[16] < reg[15] ) PC = PC + 588
			wait for 5 ns;
			assert false report "67;0x24f84663;BLT : if ( reg[16] < reg[15] ) PC = PC + 588;OK; ;" severity note;
			assert progcounter = x"00000130" report "progcounter error at step 134" severity error;
			wait for 5 ns;

		-- execute instruction 67
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 135" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 135" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 135" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 135" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 135" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 135" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 135" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 135" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 135" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 135" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 135" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 135" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 135" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 135" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 135" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 135" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 135" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 135" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 135" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 135" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 135" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 135" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 135" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 135" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 135" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 135" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 135" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 135" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 135" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 135" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 135" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 135" severity error;
			assert progcounter = x"00000134" report "progcounter error at step 135" severity error;
			wait for 5 ns;

		-- load instruction 68
			ck <= '0';
			instr <= x"24d74463"; -- BLT : if ( reg[14] < reg[13] ) PC = PC + 584
			wait for 5 ns;
			assert false report "68;0x24d74463;BLT : if ( reg[14] < reg[13] ) PC = PC + 584;OK; ;" severity note;
			assert progcounter = x"00000134" report "progcounter error at step 136" severity error;
			wait for 5 ns;

		-- execute instruction 68
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 137" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 137" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 137" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 137" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 137" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 137" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 137" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 137" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 137" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 137" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 137" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 137" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 137" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 137" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 137" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 137" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 137" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 137" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 137" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 137" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 137" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 137" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 137" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 137" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 137" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 137" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 137" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 137" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 137" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 137" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 137" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 137" severity error;
			assert progcounter = x"00000138" report "progcounter error at step 137" severity error;
			wait for 5 ns;

		-- load instruction 69
			ck <= '0';
			instr <= x"00e64463"; -- BLT : if ( reg[12] < reg[14] ) PC = PC + 8
			wait for 5 ns;
			assert false report "69;0x00e64463;BLT : if ( reg[12] < reg[14] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"00000138" report "progcounter error at step 138" severity error;
			wait for 5 ns;

		-- execute instruction 69
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 139" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 139" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 139" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 139" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 139" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 139" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 139" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 139" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 139" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 139" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 139" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 139" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 139" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 139" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 139" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 139" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 139" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 139" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 139" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 139" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 139" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 139" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 139" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 139" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 139" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 139" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 139" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 139" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 139" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 139" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 139" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 139" severity error;
			assert progcounter = x"00000140" report "progcounter error at step 139" severity error;
			wait for 5 ns;

		-- load instruction 70
			ck <= '0';
			instr <= x"0107c463"; -- BLT : if ( reg[15] < reg[16] ) PC = PC + 8
			wait for 5 ns;
			assert false report "70;0x0107c463;BLT : if ( reg[15] < reg[16] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"00000140" report "progcounter error at step 140" severity error;
			wait for 5 ns;

		-- execute instruction 70
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 141" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 141" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 141" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 141" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 141" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 141" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 141" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 141" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 141" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 141" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 141" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 141" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 141" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 141" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 141" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 141" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 141" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 141" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 141" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 141" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 141" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 141" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 141" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 141" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 141" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 141" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 141" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 141" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 141" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 141" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 141" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 141" severity error;
			assert progcounter = x"00000148" report "progcounter error at step 141" severity error;
			wait for 5 ns;

		-- load instruction 71
			ck <= '0';
			instr <= x"f8d7cce3"; -- BLT : if ( reg[15] < reg[13] ) PC = PC + -104
			wait for 5 ns;
			assert false report "71;0xf8d7cce3;BLT : if ( reg[15] < reg[13] ) PC = PC + -104;OK; ;" severity note;
			assert progcounter = x"00000148" report "progcounter error at step 142" severity error;
			wait for 5 ns;

		-- execute instruction 71
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 143" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 143" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 143" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 143" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 143" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 143" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 143" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 143" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 143" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 143" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 143" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 143" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 143" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 143" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 143" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 143" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 143" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 143" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 143" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 143" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 143" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 143" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 143" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 143" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 143" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 143" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 143" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 143" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 143" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 143" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 143" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 143" severity error;
			assert progcounter = x"000000e0" report "progcounter error at step 143" severity error;
			wait for 5 ns;

		-- load instruction 72
			ck <= '0';
			instr <= x"0700006f"; -- JAL : reg[00] = PC+4 and PC = 0xe0 + 112
			wait for 5 ns;
			assert false report "72;0x0700006f;JAL : reg[00] = PC+4 and PC = 0xe0 + 112;OK; ;" severity note;
			assert progcounter = x"000000e0" report "progcounter error at step 144" severity error;
			wait for 5 ns;

		-- execute instruction 72
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 145" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 145" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 145" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 145" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 145" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 145" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 145" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 145" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 145" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 145" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 145" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 145" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 145" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 145" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 145" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 145" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 145" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 145" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 145" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 145" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 145" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 145" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 145" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 145" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 145" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 145" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 145" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 145" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 145" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 145" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 145" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 145" severity error;
			assert progcounter = x"00000150" report "progcounter error at step 145" severity error;
			wait for 5 ns;

		-- load instruction 73
			ck <= '0';
			instr <= x"22d6e663"; -- BLTU: if ( reg[13] < reg[13] ) PC = PC + 556
			wait for 5 ns;
			assert false report "73;0x22d6e663;BLTU: if ( reg[13] < reg[13] ) PC = PC + 556;OK; ;" severity note;
			assert progcounter = x"00000150" report "progcounter error at step 146" severity error;
			wait for 5 ns;

		-- execute instruction 73
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 147" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 147" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 147" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 147" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 147" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 147" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 147" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 147" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 147" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 147" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 147" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 147" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 147" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 147" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 147" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 147" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 147" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 147" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 147" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 147" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 147" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 147" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 147" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 147" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 147" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 147" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 147" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 147" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 147" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 147" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 147" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 147" severity error;
			assert progcounter = x"00000154" report "progcounter error at step 147" severity error;
			wait for 5 ns;

		-- load instruction 74
			ck <= '0';
			instr <= x"22f86463"; -- BLTU: if ( reg[16] < reg[15] ) PC = PC + 552
			wait for 5 ns;
			assert false report "74;0x22f86463;BLTU: if ( reg[16] < reg[15] ) PC = PC + 552;OK; ;" severity note;
			assert progcounter = x"00000154" report "progcounter error at step 148" severity error;
			wait for 5 ns;

		-- execute instruction 74
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 149" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 149" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 149" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 149" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 149" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 149" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 149" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 149" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 149" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 149" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 149" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 149" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 149" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 149" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 149" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 149" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 149" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 149" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 149" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 149" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 149" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 149" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 149" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 149" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 149" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 149" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 149" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 149" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 149" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 149" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 149" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 149" severity error;
			assert progcounter = x"00000158" report "progcounter error at step 149" severity error;
			wait for 5 ns;

		-- load instruction 75
			ck <= '0';
			instr <= x"22c76263"; -- BLTU: if ( reg[14] < reg[12] ) PC = PC + 548
			wait for 5 ns;
			assert false report "75;0x22c76263;BLTU: if ( reg[14] < reg[12] ) PC = PC + 548;OK; ;" severity note;
			assert progcounter = x"00000158" report "progcounter error at step 150" severity error;
			wait for 5 ns;

		-- execute instruction 75
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 151" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 151" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 151" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 151" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 151" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 151" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 151" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 151" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 151" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 151" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 151" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 151" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 151" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 151" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 151" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 151" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 151" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 151" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 151" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 151" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 151" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 151" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 151" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 151" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 151" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 151" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 151" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 151" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 151" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 151" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 151" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 151" severity error;
			assert progcounter = x"0000015c" report "progcounter error at step 151" severity error;
			wait for 5 ns;

		-- load instruction 76
			ck <= '0';
			instr <= x"00e66463"; -- BLTU: if ( reg[12] < reg[14] ) PC = PC + 8
			wait for 5 ns;
			assert false report "76;0x00e66463;BLTU: if ( reg[12] < reg[14] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"0000015c" report "progcounter error at step 152" severity error;
			wait for 5 ns;

		-- execute instruction 76
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 153" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 153" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 153" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 153" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 153" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 153" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 153" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 153" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 153" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 153" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 153" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 153" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 153" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 153" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 153" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 153" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 153" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 153" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 153" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 153" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 153" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 153" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 153" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 153" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 153" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 153" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 153" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 153" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 153" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 153" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 153" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 153" severity error;
			assert progcounter = x"00000164" report "progcounter error at step 153" severity error;
			wait for 5 ns;

		-- load instruction 77
			ck <= '0';
			instr <= x"0107e463"; -- BLTU: if ( reg[15] < reg[16] ) PC = PC + 8
			wait for 5 ns;
			assert false report "77;0x0107e463;BLTU: if ( reg[15] < reg[16] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"00000164" report "progcounter error at step 154" severity error;
			wait for 5 ns;

		-- execute instruction 77
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 155" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 155" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 155" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 155" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 155" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 155" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 155" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 155" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 155" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 155" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 155" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 155" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 155" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 155" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 155" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 155" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 155" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 155" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 155" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 155" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 155" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 155" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 155" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 155" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 155" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 155" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 155" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 155" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 155" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 155" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 155" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 155" severity error;
			assert progcounter = x"0000016c" report "progcounter error at step 155" severity error;
			wait for 5 ns;

		-- load instruction 78
			ck <= '0';
			instr <= x"f6f6ece3"; -- BLTU: if ( reg[13] < reg[15] ) PC = PC + -136
			wait for 5 ns;
			assert false report "78;0xf6f6ece3;BLTU: if ( reg[13] < reg[15] ) PC = PC + -136;OK; ;" severity note;
			assert progcounter = x"0000016c" report "progcounter error at step 156" severity error;
			wait for 5 ns;

		-- execute instruction 78
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 157" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 157" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 157" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 157" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 157" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 157" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 157" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 157" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 157" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 157" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 157" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 157" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 157" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 157" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 157" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 157" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 157" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 157" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 157" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 157" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 157" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 157" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 157" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 157" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 157" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 157" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 157" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 157" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 157" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 157" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 157" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 157" severity error;
			assert progcounter = x"000000e4" report "progcounter error at step 157" severity error;
			wait for 5 ns;

		-- load instruction 79
			ck <= '0';
			instr <= x"0900006f"; -- JAL : reg[00] = PC+4 and PC = 0xe4 + 144
			wait for 5 ns;
			assert false report "79;0x0900006f;JAL : reg[00] = PC+4 and PC = 0xe4 + 144;OK; ;" severity note;
			assert progcounter = x"000000e4" report "progcounter error at step 158" severity error;
			wait for 5 ns;

		-- execute instruction 79
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 159" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 159" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 159" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 159" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 159" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 159" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 159" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 159" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 159" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 159" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 159" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 159" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 159" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 159" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 159" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 159" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 159" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 159" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 159" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 159" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 159" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 159" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 159" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 159" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 159" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 159" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 159" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 159" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 159" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 159" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 159" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 159" severity error;
			assert progcounter = x"00000174" report "progcounter error at step 159" severity error;
			wait for 5 ns;

		-- load instruction 80
			ck <= '0';
			instr <= x"2107d463"; -- BGE : if ( reg[15] >= reg[16] ) PC = PC + 520
			wait for 5 ns;
			assert false report "80;0x2107d463;BGE : if ( reg[15] >= reg[16] ) PC = PC + 520;OK; ;" severity note;
			assert progcounter = x"00000174" report "progcounter error at step 160" severity error;
			wait for 5 ns;

		-- execute instruction 80
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 161" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 161" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 161" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 161" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 161" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 161" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 161" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 161" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 161" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 161" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 161" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 161" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 161" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 161" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 161" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 161" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 161" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 161" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 161" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 161" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 161" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 161" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 161" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 161" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 161" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 161" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 161" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 161" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 161" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 161" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 161" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 161" severity error;
			assert progcounter = x"00000178" report "progcounter error at step 161" severity error;
			wait for 5 ns;

		-- load instruction 81
			ck <= '0';
			instr <= x"20e65263"; -- BGE : if ( reg[12] >= reg[14] ) PC = PC + 516
			wait for 5 ns;
			assert false report "81;0x20e65263;BGE : if ( reg[12] >= reg[14] ) PC = PC + 516;OK; ;" severity note;
			assert progcounter = x"00000178" report "progcounter error at step 162" severity error;
			wait for 5 ns;

		-- execute instruction 81
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 163" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 163" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 163" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 163" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 163" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 163" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 163" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 163" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 163" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 163" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 163" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 163" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 163" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 163" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 163" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 163" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 163" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 163" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 163" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 163" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 163" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 163" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 163" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 163" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 163" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 163" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 163" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 163" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 163" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 163" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 163" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 163" severity error;
			assert progcounter = x"0000017c" report "progcounter error at step 163" severity error;
			wait for 5 ns;

		-- load instruction 82
			ck <= '0';
			instr <= x"00d6d463"; -- BGE : if ( reg[13] >= reg[13] ) PC = PC + 8
			wait for 5 ns;
			assert false report "82;0x00d6d463;BGE : if ( reg[13] >= reg[13] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"0000017c" report "progcounter error at step 164" severity error;
			wait for 5 ns;

		-- execute instruction 82
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 165" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 165" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 165" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 165" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 165" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 165" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 165" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 165" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 165" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 165" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 165" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 165" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 165" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 165" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 165" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 165" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 165" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 165" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 165" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 165" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 165" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 165" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 165" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 165" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 165" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 165" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 165" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 165" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 165" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 165" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 165" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 165" severity error;
			assert progcounter = x"00000184" report "progcounter error at step 165" severity error;
			wait for 5 ns;

		-- load instruction 83
			ck <= '0';
			instr <= x"00c75463"; -- BGE : if ( reg[14] >= reg[12] ) PC = PC + 8
			wait for 5 ns;
			assert false report "83;0x00c75463;BGE : if ( reg[14] >= reg[12] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"00000184" report "progcounter error at step 166" severity error;
			wait for 5 ns;

		-- execute instruction 83
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 167" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 167" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 167" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 167" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 167" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 167" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 167" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 167" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 167" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 167" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 167" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 167" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 167" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 167" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 167" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 167" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 167" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 167" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 167" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 167" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 167" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 167" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 167" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 167" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 167" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 167" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 167" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 167" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 167" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 167" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 167" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 167" severity error;
			assert progcounter = x"0000018c" report "progcounter error at step 167" severity error;
			wait for 5 ns;

		-- load instruction 84
			ck <= '0';
			instr <= x"00f85463"; -- BGE : if ( reg[16] >= reg[15] ) PC = PC + 8
			wait for 5 ns;
			assert false report "84;0x00f85463;BGE : if ( reg[16] >= reg[15] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"0000018c" report "progcounter error at step 168" severity error;
			wait for 5 ns;

		-- execute instruction 84
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 169" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 169" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 169" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 169" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 169" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 169" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 169" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 169" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 169" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 169" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 169" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 169" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 169" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 169" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 169" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 169" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 169" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 169" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 169" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 169" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 169" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 169" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 169" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 169" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 169" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 169" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 169" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 169" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 169" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 169" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 169" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 169" severity error;
			assert progcounter = x"00000194" report "progcounter error at step 169" severity error;
			wait for 5 ns;

		-- load instruction 85
			ck <= '0';
			instr <= x"f4f6dae3"; -- BGE : if ( reg[13] >= reg[15] ) PC = PC + -172
			wait for 5 ns;
			assert false report "85;0xf4f6dae3;BGE : if ( reg[13] >= reg[15] ) PC = PC + -172;OK; ;" severity note;
			assert progcounter = x"00000194" report "progcounter error at step 170" severity error;
			wait for 5 ns;

		-- execute instruction 85
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 171" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 171" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 171" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 171" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 171" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 171" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 171" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 171" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 171" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 171" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 171" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 171" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 171" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 171" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 171" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 171" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 171" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 171" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 171" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 171" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 171" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 171" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 171" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 171" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 171" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 171" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 171" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 171" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 171" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 171" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 171" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 171" severity error;
			assert progcounter = x"000000e8" report "progcounter error at step 171" severity error;
			wait for 5 ns;

		-- load instruction 86
			ck <= '0';
			instr <= x"0b40006f"; -- JAL : reg[00] = PC+4 and PC = 0xe8 + 180
			wait for 5 ns;
			assert false report "86;0x0b40006f;JAL : reg[00] = PC+4 and PC = 0xe8 + 180;OK; ;" severity note;
			assert progcounter = x"000000e8" report "progcounter error at step 172" severity error;
			wait for 5 ns;

		-- execute instruction 86
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 173" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 173" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 173" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 173" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 173" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 173" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 173" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 173" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 173" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 173" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 173" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 173" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 173" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 173" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 173" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 173" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 173" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 173" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 173" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 173" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 173" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 173" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 173" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 173" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 173" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 173" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 173" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 173" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 173" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 173" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 173" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 173" severity error;
			assert progcounter = x"0000019c" report "progcounter error at step 173" severity error;
			wait for 5 ns;

		-- load instruction 87
			ck <= '0';
			instr <= x"1f07f063"; -- BGEU : if ( reg[15] >= reg[16] ) PC = PC + 480
			wait for 5 ns;
			assert false report "87;0x1f07f063;BGEU : if ( reg[15] >= reg[16] ) PC = PC + 480;OK; ;" severity note;
			assert progcounter = x"0000019c" report "progcounter error at step 174" severity error;
			wait for 5 ns;

		-- execute instruction 87
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 175" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 175" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 175" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 175" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 175" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 175" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 175" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 175" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 175" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 175" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 175" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 175" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 175" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 175" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 175" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 175" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 175" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 175" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 175" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 175" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 175" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 175" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 175" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 175" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 175" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 175" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 175" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 175" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 175" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 175" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 175" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 175" severity error;
			assert progcounter = x"000001a0" report "progcounter error at step 175" severity error;
			wait for 5 ns;

		-- load instruction 88
			ck <= '0';
			instr <= x"1ce67e63"; -- BGEU : if ( reg[12] >= reg[14] ) PC = PC + 476
			wait for 5 ns;
			assert false report "88;0x1ce67e63;BGEU : if ( reg[12] >= reg[14] ) PC = PC + 476;OK; ;" severity note;
			assert progcounter = x"000001a0" report "progcounter error at step 176" severity error;
			wait for 5 ns;

		-- execute instruction 88
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 177" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 177" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 177" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 177" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 177" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 177" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 177" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 177" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 177" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 177" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 177" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 177" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 177" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 177" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 177" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 177" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 177" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 177" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 177" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 177" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 177" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 177" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 177" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 177" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 177" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 177" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 177" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 177" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 177" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 177" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 177" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 177" severity error;
			assert progcounter = x"000001a4" report "progcounter error at step 177" severity error;
			wait for 5 ns;

		-- load instruction 89
			ck <= '0';
			instr <= x"00d6d463"; -- BGE : if ( reg[13] >= reg[13] ) PC = PC + 8
			wait for 5 ns;
			assert false report "89;0x00d6d463;BGE : if ( reg[13] >= reg[13] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"000001a4" report "progcounter error at step 178" severity error;
			wait for 5 ns;

		-- execute instruction 89
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 179" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 179" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 179" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 179" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 179" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 179" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 179" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 179" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 179" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 179" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 179" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 179" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 179" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 179" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 179" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 179" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 179" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 179" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 179" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 179" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 179" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 179" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 179" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 179" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 179" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 179" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 179" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 179" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 179" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 179" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 179" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 179" severity error;
			assert progcounter = x"000001ac" report "progcounter error at step 179" severity error;
			wait for 5 ns;

		-- load instruction 90
			ck <= '0';
			instr <= x"00c77463"; -- BGEU : if ( reg[14] >= reg[12] ) PC = PC + 8
			wait for 5 ns;
			assert false report "90;0x00c77463;BGEU : if ( reg[14] >= reg[12] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"000001ac" report "progcounter error at step 180" severity error;
			wait for 5 ns;

		-- execute instruction 90
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 181" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 181" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 181" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 181" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 181" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 181" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 181" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 181" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 181" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 181" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 181" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 181" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 181" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 181" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 181" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 181" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 181" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 181" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 181" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 181" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 181" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 181" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 181" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 181" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 181" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 181" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 181" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 181" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 181" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 181" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 181" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 181" severity error;
			assert progcounter = x"000001b4" report "progcounter error at step 181" severity error;
			wait for 5 ns;

		-- load instruction 91
			ck <= '0';
			instr <= x"00f87463"; -- BGEU : if ( reg[16] >= reg[15] ) PC = PC + 8
			wait for 5 ns;
			assert false report "91;0x00f87463;BGEU : if ( reg[16] >= reg[15] ) PC = PC + 8;OK; ;" severity note;
			assert progcounter = x"000001b4" report "progcounter error at step 182" severity error;
			wait for 5 ns;

		-- execute instruction 91
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 183" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 183" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 183" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 183" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 183" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 183" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 183" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 183" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 183" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 183" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 183" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 183" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 183" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 183" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 183" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 183" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 183" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 183" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 183" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 183" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 183" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 183" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 183" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 183" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 183" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 183" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 183" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 183" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 183" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 183" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 183" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 183" severity error;
			assert progcounter = x"000001bc" report "progcounter error at step 183" severity error;
			wait for 5 ns;

		-- load instruction 92
			ck <= '0';
			instr <= x"f2d7f8e3"; -- BGEU : if ( reg[15] >= reg[13] ) PC = PC + -208
			wait for 5 ns;
			assert false report "92;0xf2d7f8e3;BGEU : if ( reg[15] >= reg[13] ) PC = PC + -208;OK; ;" severity note;
			assert progcounter = x"000001bc" report "progcounter error at step 184" severity error;
			wait for 5 ns;

		-- execute instruction 92
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 185" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 185" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 185" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 185" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 185" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 185" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 185" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 185" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 185" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 185" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 185" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 185" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 185" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 185" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 185" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 185" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 185" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 185" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 185" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 185" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 185" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 185" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 185" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 185" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 185" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 185" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 185" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 185" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 185" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 185" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 185" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 185" severity error;
			assert progcounter = x"000000ec" report "progcounter error at step 185" severity error;
			wait for 5 ns;

		-- load instruction 93
			ck <= '0';
			instr <= x"0d80006f"; -- JAL : reg[00] = PC+4 and PC = 0xec + 216
			wait for 5 ns;
			assert false report "93;0x0d80006f;JAL : reg[00] = PC+4 and PC = 0xec + 216;OK; ;" severity note;
			assert progcounter = x"000000ec" report "progcounter error at step 186" severity error;
			wait for 5 ns;

		-- execute instruction 93
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 187" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 187" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 187" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 187" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 187" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 187" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 187" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 187" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 187" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 187" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 187" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 187" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 187" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 187" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 187" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 187" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 187" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 187" severity error;
			assert reg12 = x"fffff000" report "reg12 error at step 187" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 187" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 187" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 187" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 187" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 187" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 187" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 187" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 187" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 187" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 187" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 187" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 187" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 187" severity error;
			assert progcounter = x"000001c4" report "progcounter error at step 187" severity error;
			wait for 5 ns;

		-- load instruction 94
			ck <= '0';
			instr <= x"20500913"; -- ADDI : reg[18] = reg[00] + 517
			wait for 5 ns;
			assert false report "94;0x20500913;ADDI : reg[18] = reg[00] + 517;OK; ;" severity note;
			assert progcounter = x"000001c4" report "progcounter error at step 188" severity error;
			wait for 5 ns;

		-- execute instruction 94
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 189" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 189" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 189" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 189" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 189" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 189" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 189" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 189" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 189" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 189" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 189" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 189" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 189" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 189" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 189" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 189" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 189" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 189" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 189" severity error;
			assert reg13 = x"fffff000" report "reg13 error at step 189" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 189" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 189" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 189" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 189" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 189" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 189" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 189" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 189" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 189" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 189" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 189" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 189" severity error;
			assert progcounter = x"000001c8" report "progcounter error at step 189" severity error;
			wait for 5 ns;

		-- load instruction 95
			ck <= '0';
			instr <= x"20100993"; -- ADDI : reg[19] = reg[00] + 513
			wait for 5 ns;
			assert false report "95;0x20100993;ADDI : reg[19] = reg[00] + 513;OK; ;" severity note;
			assert progcounter = x"000001c8" report "progcounter error at step 190" severity error;
			wait for 5 ns;

		-- execute instruction 95
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 191" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 191" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 191" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 191" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 191" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 191" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 191" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 191" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 191" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 191" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 191" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 191" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 191" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 191" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 191" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 191" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 191" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 191" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 191" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 191" severity error;
			assert reg14 = x"fffff000" report "reg14 error at step 191" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 191" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 191" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 191" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 191" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 191" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 191" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 191" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 191" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 191" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 191" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 191" severity error;
			assert progcounter = x"000001cc" report "progcounter error at step 191" severity error;
			wait for 5 ns;

		-- load instruction 96
			ck <= '0';
			instr <= x"00001a37"; -- LUI : reg[20] = 0x1 << 12
			wait for 5 ns;
			assert false report "96;0x00001a37;LUI : reg[20] = 0x1 << 12;OK; ;" severity note;
			assert progcounter = x"000001cc" report "progcounter error at step 192" severity error;
			wait for 5 ns;

		-- execute instruction 96
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 193" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 193" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 193" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 193" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 193" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 193" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 193" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 193" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 193" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 193" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 193" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 193" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 193" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 193" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 193" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 193" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 193" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 193" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 193" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 193" severity error;
			assert reg14 = x"00001000" report "reg14 error at step 193" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 193" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 193" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 193" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 193" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 193" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 193" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 193" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 193" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 193" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 193" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 193" severity error;
			assert progcounter = x"000001d0" report "progcounter error at step 193" severity error;
			wait for 5 ns;

		-- load instruction 97
			ck <= '0';
			instr <= x"400a0a13"; -- ADDI : reg[20] = reg[20] + 1024
			wait for 5 ns;
			assert false report "97;0x400a0a13;ADDI : reg[20] = reg[20] + 1024;OK; ;" severity note;
			assert progcounter = x"000001d0" report "progcounter error at step 194" severity error;
			wait for 5 ns;

		-- execute instruction 97
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 195" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 195" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 195" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 195" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 195" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 195" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 195" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 195" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 195" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 195" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 195" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 195" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 195" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 195" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 195" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 195" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 195" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 195" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 195" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 195" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 195" severity error;
			assert reg15 = x"fffff000" report "reg15 error at step 195" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 195" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 195" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 195" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 195" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 195" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 195" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 195" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 195" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 195" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 195" severity error;
			assert progcounter = x"000001d4" report "progcounter error at step 195" severity error;
			wait for 5 ns;

		-- load instruction 98
			ck <= '0';
			instr <= x"80808ab7"; -- LUI : reg[21] = 0x80808 << 12
			wait for 5 ns;
			assert false report "98;0x80808ab7;LUI : reg[21] = 0x80808 << 12;OK; ;" severity note;
			assert progcounter = x"000001d4" report "progcounter error at step 196" severity error;
			wait for 5 ns;

		-- execute instruction 98
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 197" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 197" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 197" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 197" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 197" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 197" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 197" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 197" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 197" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 197" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 197" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 197" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 197" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 197" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 197" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 197" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 197" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 197" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 197" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 197" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 197" severity error;
			assert reg15 = x"80808000" report "reg15 error at step 197" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 197" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 197" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 197" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 197" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 197" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 197" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 197" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 197" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 197" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 197" severity error;
			assert progcounter = x"000001d8" report "progcounter error at step 197" severity error;
			wait for 5 ns;

		-- load instruction 99
			ck <= '0';
			instr <= x"080a8a93"; -- ADDI : reg[21] = reg[21] + 128
			wait for 5 ns;
			assert false report "99;0x080a8a93;ADDI : reg[21] = reg[21] + 128;OK; ;" severity note;
			assert progcounter = x"000001d8" report "progcounter error at step 198" severity error;
			wait for 5 ns;

		-- execute instruction 99
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 199" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 199" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 199" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 199" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 199" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 199" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 199" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 199" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 199" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 199" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 199" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 199" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 199" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 199" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 199" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 199" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 199" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 199" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 199" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 199" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 199" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 199" severity error;
			assert reg16 = x"fffff000" report "reg16 error at step 199" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 199" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 199" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 199" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 199" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 199" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 199" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 199" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 199" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 199" severity error;
			assert progcounter = x"000001dc" report "progcounter error at step 199" severity error;
			wait for 5 ns;

		-- load instruction 100
			ck <= '0';
			instr <= x"11223b37"; -- LUI : reg[22] = 0x11223 << 12
			wait for 5 ns;
			assert false report "100;0x11223b37;LUI : reg[22] = 0x11223 << 12;OK; ;" severity note;
			assert progcounter = x"000001dc" report "progcounter error at step 200" severity error;
			wait for 5 ns;

		-- execute instruction 100
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 201" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 201" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 201" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 201" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 201" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 201" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 201" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 201" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 201" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 201" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 201" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 201" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 201" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 201" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 201" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 201" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 201" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 201" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 201" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 201" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 201" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 201" severity error;
			assert reg16 = x"11223000" report "reg16 error at step 201" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 201" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 201" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 201" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 201" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 201" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 201" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 201" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 201" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 201" severity error;
			assert progcounter = x"000001e0" report "progcounter error at step 201" severity error;
			wait for 5 ns;

		-- load instruction 101
			ck <= '0';
			instr <= x"344b0b13"; -- ADDI : reg[22] = reg[22] + 836
			wait for 5 ns;
			assert false report "101;0x344b0b13;ADDI : reg[22] = reg[22] + 836;OK; ;" severity note;
			assert progcounter = x"000001e0" report "progcounter error at step 202" severity error;
			wait for 5 ns;

		-- execute instruction 101
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 203" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 203" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 203" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 203" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 203" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 203" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 203" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 203" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 203" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 203" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 203" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 203" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 203" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 203" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 203" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 203" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 203" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 203" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 203" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 203" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 203" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 203" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 203" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 203" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 203" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 203" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 203" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 203" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 203" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 203" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 203" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 203" severity error;
			assert progcounter = x"000001e4" report "progcounter error at step 203" severity error;
			wait for 5 ns;

		-- load instruction 102
			ck <= '0';
			instr <= x"815a0023"; -- STRB : dataMem[reg[20] + -2048] = reg[21]
			wait for 5 ns;
			assert false report "102;0x815a0023;STRB : dataMem[reg[20] + -2048] = reg[21];OK; ;" severity note;
			assert progcounter = x"000001e4" report "progcounter error at step 204" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 204"     severity error;
			assert inputData = x"80808080"   report "data error at step  204"       severity error;
			assert dataLength = "000"        report "length error at step 204"      severity error;
			assert store = '1'               report "store error at step 204"       severity error;
			wait for 5 ns;

		-- execute instruction 102
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 205" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 205" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 205" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 205" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 205" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 205" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 205" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 205" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 205" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 205" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 205" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 205" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 205" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 205" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 205" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 205" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 205" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 205" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 205" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 205" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 205" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 205" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 205" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 205" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 205" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 205" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 205" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 205" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 205" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 205" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 205" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 205" severity error;
			assert progcounter = x"000001e8" report "progcounter error at step 205" severity error;
			wait for 5 ns;

		-- load instruction 103
			ck <= '0';
			instr <= x"7f698fa3"; -- STRB : dataMem[reg[19] + 2047] = reg[22]
			wait for 5 ns;
			assert false report "103;0x7f698fa3;STRB : dataMem[reg[19] + 2047] = reg[22];OK; ;" severity note;
			assert progcounter = x"000001e8" report "progcounter error at step 206" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 206"     severity error;
			assert inputData = x"11223344"   report "data error at step  206"       severity error;
			assert dataLength = "000"        report "length error at step 206"      severity error;
			assert store = '1'               report "store error at step 206"       severity error;
			wait for 5 ns;

		-- execute instruction 103
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 207" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 207" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 207" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 207" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 207" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 207" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 207" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 207" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 207" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 207" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 207" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 207" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 207" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 207" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 207" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 207" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 207" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 207" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 207" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 207" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 207" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 207" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 207" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 207" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 207" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 207" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 207" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 207" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 207" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 207" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 207" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 207" severity error;
			assert progcounter = x"000001ec" report "progcounter error at step 207" severity error;
			wait for 5 ns;

		-- load instruction 104
			ck <= '0';
			instr <= x"815a1023"; -- STRH : dataMem[reg[20] + -2048] = reg[21]
			wait for 5 ns;
			assert false report "104;0x815a1023;STRH : dataMem[reg[20] + -2048] = reg[21];OK; ;" severity note;
			assert progcounter = x"000001ec" report "progcounter error at step 208" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 208"     severity error;
			assert inputData = x"80808080"   report "data error at step  208"       severity error;
			assert dataLength = "001"        report "length error at step 208"      severity error;
			assert store = '1'               report "store error at step 208"       severity error;
			wait for 5 ns;

		-- execute instruction 104
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 209" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 209" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 209" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 209" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 209" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 209" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 209" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 209" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 209" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 209" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 209" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 209" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 209" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 209" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 209" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 209" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 209" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 209" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 209" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 209" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 209" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 209" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 209" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 209" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 209" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 209" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 209" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 209" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 209" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 209" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 209" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 209" severity error;
			assert progcounter = x"000001f0" report "progcounter error at step 209" severity error;
			wait for 5 ns;

		-- load instruction 105
			ck <= '0';
			instr <= x"7f699fa3"; -- STRH : dataMem[reg[19] + 2047] = reg[22]
			wait for 5 ns;
			assert false report "105;0x7f699fa3;STRH : dataMem[reg[19] + 2047] = reg[22];OK; ;" severity note;
			assert progcounter = x"000001f0" report "progcounter error at step 210" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 210"     severity error;
			assert inputData = x"11223344"   report "data error at step  210"       severity error;
			assert dataLength = "001"        report "length error at step 210"      severity error;
			assert store = '1'               report "store error at step 210"       severity error;
			wait for 5 ns;

		-- execute instruction 105
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 211" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 211" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 211" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 211" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 211" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 211" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 211" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 211" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 211" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 211" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 211" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 211" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 211" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 211" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 211" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 211" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 211" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 211" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 211" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 211" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 211" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 211" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 211" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 211" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 211" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 211" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 211" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 211" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 211" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 211" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 211" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 211" severity error;
			assert progcounter = x"000001f4" report "progcounter error at step 211" severity error;
			wait for 5 ns;

		-- load instruction 106
			ck <= '0';
			instr <= x"815a2023"; -- STRW : dataMem[reg[20] + -2048] = reg[21]
			wait for 5 ns;
			assert false report "106;0x815a2023;STRW : dataMem[reg[20] + -2048] = reg[21];OK; ;" severity note;
			assert progcounter = x"000001f4" report "progcounter error at step 212" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 212"     severity error;
			assert inputData = x"80808080"   report "data error at step  212"       severity error;
			assert dataLength = "010"        report "length error at step 212"      severity error;
			assert store = '1'               report "store error at step 212"       severity error;
			wait for 5 ns;

		-- execute instruction 106
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 213" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 213" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 213" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 213" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 213" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 213" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 213" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 213" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 213" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 213" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 213" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 213" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 213" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 213" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 213" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 213" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 213" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 213" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 213" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 213" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 213" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 213" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 213" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 213" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 213" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 213" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 213" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 213" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 213" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 213" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 213" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 213" severity error;
			assert progcounter = x"000001f8" report "progcounter error at step 213" severity error;
			wait for 5 ns;

		-- load instruction 107
			ck <= '0';
			instr <= x"7f69afa3"; -- STRW : dataMem[reg[19] + 2047] = reg[22]
			wait for 5 ns;
			assert false report "107;0x7f69afa3;STRW : dataMem[reg[19] + 2047] = reg[22];OK; ;" severity note;
			assert progcounter = x"000001f8" report "progcounter error at step 214" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 214"     severity error;
			assert inputData = x"11223344"   report "data error at step  214"       severity error;
			assert dataLength = "010"        report "length error at step 214"      severity error;
			assert store = '1'               report "store error at step 214"       severity error;
			wait for 5 ns;

		-- execute instruction 107
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 215" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 215" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 215" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 215" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 215" severity error;
			assert reg05 = x"fffff000" report "reg05 error at step 215" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 215" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 215" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 215" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 215" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 215" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 215" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 215" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 215" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 215" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 215" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 215" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 215" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 215" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 215" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 215" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 215" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 215" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 215" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 215" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 215" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 215" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 215" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 215" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 215" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 215" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 215" severity error;
			assert progcounter = x"000001fc" report "progcounter error at step 215" severity error;
			wait for 5 ns;

		-- load instruction 108
			ck <= '0';
			instr <= x"800a0283"; -- LDB : reg[05] = dataMem[reg[20] + -2048]
			outputData <= x"ffffff80";
			wait for 5 ns;
			assert false report "108;0x800a0283;LDB : reg[05] = dataMem[reg[20] + -2048];OK; ;" severity note;
			assert progcounter = x"000001fc" report "progcounter error at step 216" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 216"     severity error;
			assert dataLength = "000"        report "length error at step 216"      severity error;
			assert load = '1'                report "load error at step 216"        severity error;
			wait for 5 ns;

		-- execute instruction 108
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 217" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 217" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 217" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 217" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 217" severity error;
			assert reg05 = x"ffffff80" report "reg05 error at step 217" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 217" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 217" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 217" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 217" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 217" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 217" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 217" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 217" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 217" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 217" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 217" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 217" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 217" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 217" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 217" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 217" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 217" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 217" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 217" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 217" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 217" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 217" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 217" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 217" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 217" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 217" severity error;
			assert progcounter = x"00000200" report "progcounter error at step 217" severity error;
			wait for 5 ns;

		-- load instruction 109
			ck <= '0';
			instr <= x"7ff98283"; -- LDB : reg[05] = dataMem[reg[19] + 2047]
			outputData <= x"00000044";
			wait for 5 ns;
			assert false report "109;0x7ff98283;LDB : reg[05] = dataMem[reg[19] + 2047];OK; ;" severity note;
			assert progcounter = x"00000200" report "progcounter error at step 218" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 218"     severity error;
			assert dataLength = "000"        report "length error at step 218"      severity error;
			assert load = '1'                report "load error at step 218"        severity error;
			wait for 5 ns;

		-- execute instruction 109
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 219" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 219" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 219" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 219" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 219" severity error;
			assert reg05 = x"00000044" report "reg05 error at step 219" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 219" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 219" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 219" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 219" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 219" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 219" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 219" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 219" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 219" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 219" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 219" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 219" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 219" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 219" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 219" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 219" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 219" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 219" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 219" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 219" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 219" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 219" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 219" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 219" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 219" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 219" severity error;
			assert progcounter = x"00000204" report "progcounter error at step 219" severity error;
			wait for 5 ns;

		-- load instruction 110
			ck <= '0';
			instr <= x"800a4283"; -- LBU : reg[05] = dataMem[reg[20] + -2048]
			outputData <= x"00000080";
			wait for 5 ns;
			assert false report "110;0x800a4283;LBU : reg[05] = dataMem[reg[20] + -2048];OK; ;" severity note;
			assert progcounter = x"00000204" report "progcounter error at step 220" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 220"     severity error;
			assert dataLength = "100"        report "length error at step 220"      severity error;
			assert load = '1'                report "load error at step 220"        severity error;
			wait for 5 ns;

		-- execute instruction 110
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 221" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 221" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 221" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 221" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 221" severity error;
			assert reg05 = x"00000080" report "reg05 error at step 221" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 221" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 221" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 221" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 221" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 221" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 221" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 221" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 221" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 221" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 221" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 221" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 221" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 221" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 221" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 221" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 221" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 221" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 221" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 221" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 221" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 221" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 221" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 221" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 221" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 221" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 221" severity error;
			assert progcounter = x"00000208" report "progcounter error at step 221" severity error;
			wait for 5 ns;

		-- load instruction 111
			ck <= '0';
			instr <= x"7ff9c283"; -- LBU : reg[05] = dataMem[reg[19] + 2047]
			outputData <= x"00000044";
			wait for 5 ns;
			assert false report "111;0x7ff9c283;LBU : reg[05] = dataMem[reg[19] + 2047];OK; ;" severity note;
			assert progcounter = x"00000208" report "progcounter error at step 222" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 222"     severity error;
			assert dataLength = "100"        report "length error at step 222"      severity error;
			assert load = '1'                report "load error at step 222"        severity error;
			wait for 5 ns;

		-- execute instruction 111
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 223" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 223" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 223" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 223" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 223" severity error;
			assert reg05 = x"00000044" report "reg05 error at step 223" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 223" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 223" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 223" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 223" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 223" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 223" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 223" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 223" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 223" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 223" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 223" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 223" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 223" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 223" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 223" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 223" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 223" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 223" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 223" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 223" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 223" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 223" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 223" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 223" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 223" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 223" severity error;
			assert progcounter = x"0000020c" report "progcounter error at step 223" severity error;
			wait for 5 ns;

		-- load instruction 112
			ck <= '0';
			instr <= x"800a1283"; -- LDH : reg[05] = dataMem[reg[20] + -2048]
			outputData <= x"ffff8080";
			wait for 5 ns;
			assert false report "112;0x800a1283;LDH : reg[05] = dataMem[reg[20] + -2048];OK; ;" severity note;
			assert progcounter = x"0000020c" report "progcounter error at step 224" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 224"     severity error;
			assert dataLength = "001"        report "length error at step 224"      severity error;
			assert load = '1'                report "load error at step 224"        severity error;
			wait for 5 ns;

		-- execute instruction 112
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 225" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 225" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 225" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 225" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 225" severity error;
			assert reg05 = x"ffff8080" report "reg05 error at step 225" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 225" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 225" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 225" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 225" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 225" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 225" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 225" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 225" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 225" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 225" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 225" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 225" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 225" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 225" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 225" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 225" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 225" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 225" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 225" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 225" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 225" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 225" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 225" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 225" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 225" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 225" severity error;
			assert progcounter = x"00000210" report "progcounter error at step 225" severity error;
			wait for 5 ns;

		-- load instruction 113
			ck <= '0';
			instr <= x"7ff99283"; -- LDH : reg[05] = dataMem[reg[19] + 2047]
			outputData <= x"00003344";
			wait for 5 ns;
			assert false report "113;0x7ff99283;LDH : reg[05] = dataMem[reg[19] + 2047];OK; ;" severity note;
			assert progcounter = x"00000210" report "progcounter error at step 226" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 226"     severity error;
			assert dataLength = "001"        report "length error at step 226"      severity error;
			assert load = '1'                report "load error at step 226"        severity error;
			wait for 5 ns;

		-- execute instruction 113
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 227" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 227" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 227" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 227" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 227" severity error;
			assert reg05 = x"00003344" report "reg05 error at step 227" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 227" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 227" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 227" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 227" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 227" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 227" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 227" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 227" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 227" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 227" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 227" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 227" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 227" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 227" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 227" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 227" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 227" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 227" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 227" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 227" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 227" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 227" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 227" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 227" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 227" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 227" severity error;
			assert progcounter = x"00000214" report "progcounter error at step 227" severity error;
			wait for 5 ns;

		-- load instruction 114
			ck <= '0';
			instr <= x"800a5283"; -- LHU : reg[05] = dataMem[reg[20] + -2048]
			outputData <= x"00008080";
			wait for 5 ns;
			assert false report "114;0x800a5283;LHU : reg[05] = dataMem[reg[20] + -2048];OK; ;" severity note;
			assert progcounter = x"00000214" report "progcounter error at step 228" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 228"     severity error;
			assert dataLength = "101"        report "length error at step 228"      severity error;
			assert load = '1'                report "load error at step 228"        severity error;
			wait for 5 ns;

		-- execute instruction 114
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 229" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 229" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 229" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 229" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 229" severity error;
			assert reg05 = x"00008080" report "reg05 error at step 229" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 229" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 229" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 229" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 229" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 229" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 229" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 229" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 229" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 229" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 229" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 229" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 229" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 229" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 229" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 229" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 229" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 229" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 229" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 229" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 229" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 229" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 229" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 229" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 229" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 229" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 229" severity error;
			assert progcounter = x"00000218" report "progcounter error at step 229" severity error;
			wait for 5 ns;

		-- load instruction 115
			ck <= '0';
			instr <= x"7ff9d283"; -- LHU : reg[05] = dataMem[reg[19] + 2047]
			outputData <= x"00003344";
			wait for 5 ns;
			assert false report "115;0x7ff9d283;LHU : reg[05] = dataMem[reg[19] + 2047];OK; ;" severity note;
			assert progcounter = x"00000218" report "progcounter error at step 230" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 230"     severity error;
			assert dataLength = "101"        report "length error at step 230"      severity error;
			assert load = '1'                report "load error at step 230"        severity error;
			wait for 5 ns;

		-- execute instruction 115
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 231" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 231" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 231" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 231" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 231" severity error;
			assert reg05 = x"00003344" report "reg05 error at step 231" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 231" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 231" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 231" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 231" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 231" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 231" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 231" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 231" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 231" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 231" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 231" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 231" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 231" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 231" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 231" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 231" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 231" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 231" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 231" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 231" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 231" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 231" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 231" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 231" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 231" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 231" severity error;
			assert progcounter = x"0000021c" report "progcounter error at step 231" severity error;
			wait for 5 ns;

		-- load instruction 116
			ck <= '0';
			instr <= x"800a2283"; -- LDW : reg[05] = dataMem[reg[20] + -2048]
			outputData <= x"80808080";
			wait for 5 ns;
			assert false report "116;0x800a2283;LDW : reg[05] = dataMem[reg[20] + -2048];OK; ;" severity note;
			assert progcounter = x"0000021c" report "progcounter error at step 232" severity error;
			assert dataAddr = x"00000c00"    report "address error at step 232"     severity error;
			assert dataLength = "010"        report "length error at step 232"      severity error;
			assert load = '1'                report "load error at step 232"        severity error;
			wait for 5 ns;

		-- execute instruction 116
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 233" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 233" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 233" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 233" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 233" severity error;
			assert reg05 = x"80808080" report "reg05 error at step 233" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 233" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 233" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 233" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 233" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 233" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 233" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 233" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 233" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 233" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 233" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 233" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 233" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 233" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 233" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 233" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 233" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 233" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 233" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 233" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 233" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 233" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 233" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 233" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 233" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 233" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 233" severity error;
			assert progcounter = x"00000220" report "progcounter error at step 233" severity error;
			wait for 5 ns;

		-- load instruction 117
			ck <= '0';
			instr <= x"7ff9a283"; -- LDW : reg[05] = dataMem[reg[19] + 2047]
			outputData <= x"11223344";
			wait for 5 ns;
			assert false report "117;0x7ff9a283;LDW : reg[05] = dataMem[reg[19] + 2047];OK; ;" severity note;
			assert progcounter = x"00000220" report "progcounter error at step 234" severity error;
			assert dataAddr = x"00000a00"    report "address error at step 234"     severity error;
			assert dataLength = "010"        report "length error at step 234"      severity error;
			assert load = '1'                report "load error at step 234"        severity error;
			wait for 5 ns;

		-- execute instruction 117
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 235" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 235" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 235" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 235" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 235" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 235" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 235" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 235" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 235" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 235" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 235" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 235" severity error;
			assert reg0c = x"00000003" report "reg0c error at step 235" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 235" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 235" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 235" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 235" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 235" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 235" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 235" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 235" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 235" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 235" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 235" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 235" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 235" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 235" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 235" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 235" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 235" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 235" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 235" severity error;
			assert progcounter = x"00000224" report "progcounter error at step 235" severity error;
			wait for 5 ns;

		-- load instruction 118
			ck <= '0';
			instr <= x"80000613"; -- ADDI : reg[12] = reg[00] + -2048
			wait for 5 ns;
			assert false report "118;0x80000613;ADDI : reg[12] = reg[00] + -2048;OK; ;" severity note;
			assert progcounter = x"00000224" report "progcounter error at step 236" severity error;
			wait for 5 ns;

		-- execute instruction 118
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 237" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 237" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 237" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 237" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 237" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 237" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 237" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 237" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 237" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 237" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 237" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 237" severity error;
			assert reg0c = x"fffff800" report "reg0c error at step 237" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 237" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 237" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 237" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 237" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 237" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 237" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 237" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 237" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 237" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 237" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 237" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 237" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 237" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 237" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 237" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 237" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 237" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 237" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 237" severity error;
			assert progcounter = x"00000228" report "progcounter error at step 237" severity error;
			wait for 5 ns;

		-- load instruction 119
			ck <= '0';
			instr <= x"7ff60613"; -- ADDI : reg[12] = reg[12] + 2047
			wait for 5 ns;
			assert false report "119;0x7ff60613;ADDI : reg[12] = reg[12] + 2047;OK; ;" severity note;
			assert progcounter = x"00000228" report "progcounter error at step 238" severity error;
			wait for 5 ns;

		-- execute instruction 119
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 239" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 239" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 239" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 239" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 239" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 239" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 239" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 239" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 239" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 239" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 239" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 239" severity error;
			assert reg0c = x"ffffffff" report "reg0c error at step 239" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 239" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 239" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 239" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 239" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 239" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 239" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 239" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 239" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 239" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 239" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 239" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 239" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 239" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 239" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 239" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 239" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 239" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 239" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 239" severity error;
			assert progcounter = x"0000022c" report "progcounter error at step 239" severity error;
			wait for 5 ns;

		-- load instruction 120
			ck <= '0';
			instr <= x"7ff60613"; -- ADDI : reg[12] = reg[12] + 2047
			wait for 5 ns;
			assert false report "120;0x7ff60613;ADDI : reg[12] = reg[12] + 2047;OK; ;" severity note;
			assert progcounter = x"0000022c" report "progcounter error at step 240" severity error;
			wait for 5 ns;

		-- execute instruction 120
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 241" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 241" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 241" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 241" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 241" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 241" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 241" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 241" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 241" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 241" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 241" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 241" severity error;
			assert reg0c = x"000007fe" report "reg0c error at step 241" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 241" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 241" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 241" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 241" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 241" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 241" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 241" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 241" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 241" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 241" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 241" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 241" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 241" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 241" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 241" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 241" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 241" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 241" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 241" severity error;
			assert progcounter = x"00000230" report "progcounter error at step 241" severity error;
			wait for 5 ns;

		-- load instruction 121
			ck <= '0';
			instr <= x"00000613"; -- ADDI : reg[12] = reg[00] + 0
			wait for 5 ns;
			assert false report "121;0x00000613;ADDI : reg[12] = reg[00] + 0;OK; ;" severity note;
			assert progcounter = x"00000230" report "progcounter error at step 242" severity error;
			wait for 5 ns;

		-- execute instruction 121
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 243" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 243" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 243" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 243" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 243" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 243" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 243" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 243" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 243" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 243" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 243" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 243" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 243" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 243" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 243" severity error;
			assert reg0f = x"fffffffb" report "reg0f error at step 243" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 243" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 243" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 243" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 243" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 243" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 243" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 243" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 243" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 243" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 243" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 243" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 243" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 243" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 243" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 243" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 243" severity error;
			assert progcounter = x"00000234" report "progcounter error at step 243" severity error;
			wait for 5 ns;

		-- load instruction 122
			ck <= '0';
			instr <= x"00600793"; -- ADDI : reg[15] = reg[00] + 6
			wait for 5 ns;
			assert false report "122;0x00600793;ADDI : reg[15] = reg[00] + 6;OK; ;" severity note;
			assert progcounter = x"00000234" report "progcounter error at step 244" severity error;
			wait for 5 ns;

		-- execute instruction 122
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 245" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 245" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 245" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 245" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 245" severity error;
			assert reg05 = x"11223344" report "reg05 error at step 245" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 245" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 245" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 245" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 245" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 245" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 245" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 245" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 245" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 245" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 245" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 245" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 245" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 245" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 245" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 245" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 245" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 245" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 245" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 245" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 245" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 245" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 245" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 245" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 245" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 245" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 245" severity error;
			assert progcounter = x"00000238" report "progcounter error at step 245" severity error;
			wait for 5 ns;

		-- load instruction 123
			ck <= '0';
			instr <= x"0067a293"; -- SLTI : reg[05] = ( reg[15] < 6 ) ? 1 : 0
			wait for 5 ns;
			assert false report "123;0x0067a293;SLTI : reg[05] = ( reg[15] < 6 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000238" report "progcounter error at step 246" severity error;
			wait for 5 ns;

		-- execute instruction 123
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 247" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 247" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 247" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 247" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 247" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 247" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 247" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 247" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 247" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 247" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 247" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 247" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 247" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 247" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 247" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 247" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 247" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 247" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 247" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 247" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 247" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 247" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 247" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 247" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 247" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 247" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 247" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 247" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 247" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 247" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 247" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 247" severity error;
			assert progcounter = x"0000023c" report "progcounter error at step 247" severity error;
			wait for 5 ns;

		-- load instruction 124
			ck <= '0';
			instr <= x"ff97a293"; -- SLTI : reg[05] = ( reg[15] < -7 ) ? 1 : 0
			wait for 5 ns;
			assert false report "124;0xff97a293;SLTI : reg[05] = ( reg[15] < -7 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"0000023c" report "progcounter error at step 248" severity error;
			wait for 5 ns;

		-- execute instruction 124
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 249" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 249" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 249" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 249" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 249" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 249" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 249" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 249" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 249" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 249" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 249" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 249" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 249" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 249" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 249" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 249" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 249" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 249" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 249" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 249" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 249" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 249" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 249" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 249" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 249" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 249" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 249" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 249" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 249" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 249" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 249" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 249" severity error;
			assert progcounter = x"00000240" report "progcounter error at step 249" severity error;
			wait for 5 ns;

		-- load instruction 125
			ck <= '0';
			instr <= x"0077a293"; -- SLTI : reg[05] = ( reg[15] < 7 ) ? 1 : 0
			wait for 5 ns;
			assert false report "125;0x0077a293;SLTI : reg[05] = ( reg[15] < 7 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000240" report "progcounter error at step 250" severity error;
			wait for 5 ns;

		-- execute instruction 125
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 251" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 251" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 251" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 251" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 251" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 251" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 251" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 251" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 251" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 251" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 251" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 251" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 251" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 251" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 251" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 251" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 251" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 251" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 251" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 251" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 251" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 251" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 251" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 251" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 251" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 251" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 251" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 251" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 251" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 251" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 251" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 251" severity error;
			assert progcounter = x"00000244" report "progcounter error at step 251" severity error;
			wait for 5 ns;

		-- load instruction 126
			ck <= '0';
			instr <= x"ffa00793"; -- ADDI : reg[15] = reg[00] + -6
			wait for 5 ns;
			assert false report "126;0xffa00793;ADDI : reg[15] = reg[00] + -6;OK; ;" severity note;
			assert progcounter = x"00000244" report "progcounter error at step 252" severity error;
			wait for 5 ns;

		-- execute instruction 126
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 253" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 253" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 253" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 253" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 253" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 253" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 253" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 253" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 253" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 253" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 253" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 253" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 253" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 253" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 253" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 253" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 253" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 253" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 253" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 253" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 253" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 253" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 253" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 253" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 253" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 253" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 253" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 253" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 253" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 253" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 253" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 253" severity error;
			assert progcounter = x"00000248" report "progcounter error at step 253" severity error;
			wait for 5 ns;

		-- load instruction 127
			ck <= '0';
			instr <= x"ff97a293"; -- SLTI : reg[05] = ( reg[15] < -7 ) ? 1 : 0
			wait for 5 ns;
			assert false report "127;0xff97a293;SLTI : reg[05] = ( reg[15] < -7 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000248" report "progcounter error at step 254" severity error;
			wait for 5 ns;

		-- execute instruction 127
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 255" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 255" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 255" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 255" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 255" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 255" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 255" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 255" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 255" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 255" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 255" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 255" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 255" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 255" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 255" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 255" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 255" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 255" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 255" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 255" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 255" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 255" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 255" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 255" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 255" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 255" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 255" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 255" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 255" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 255" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 255" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 255" severity error;
			assert progcounter = x"0000024c" report "progcounter error at step 255" severity error;
			wait for 5 ns;

		-- load instruction 128
			ck <= '0';
			instr <= x"0007a293"; -- SLTI : reg[05] = ( reg[15] < 0 ) ? 1 : 0
			wait for 5 ns;
			assert false report "128;0x0007a293;SLTI : reg[05] = ( reg[15] < 0 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"0000024c" report "progcounter error at step 256" severity error;
			wait for 5 ns;

		-- execute instruction 128
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 257" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 257" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 257" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 257" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 257" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 257" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 257" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 257" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 257" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 257" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 257" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 257" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 257" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 257" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 257" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 257" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 257" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 257" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 257" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 257" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 257" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 257" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 257" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 257" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 257" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 257" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 257" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 257" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 257" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 257" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 257" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 257" severity error;
			assert progcounter = x"00000250" report "progcounter error at step 257" severity error;
			wait for 5 ns;

		-- load instruction 129
			ck <= '0';
			instr <= x"0067b293"; -- SLTIU : reg[05] = ( reg[15] < 6 ) ? 1 : 0
			wait for 5 ns;
			assert false report "129;0x0067b293;SLTIU : reg[05] = ( reg[15] < 6 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000250" report "progcounter error at step 258" severity error;
			wait for 5 ns;

		-- execute instruction 129
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 259" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 259" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 259" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 259" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 259" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 259" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 259" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 259" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 259" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 259" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 259" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 259" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 259" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 259" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 259" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 259" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 259" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 259" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 259" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 259" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 259" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 259" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 259" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 259" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 259" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 259" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 259" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 259" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 259" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 259" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 259" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 259" severity error;
			assert progcounter = x"00000254" report "progcounter error at step 259" severity error;
			wait for 5 ns;

		-- load instruction 130
			ck <= '0';
			instr <= x"ff97b293"; -- SLTIU : reg[05] = ( reg[15] < -7 ) ? 1 : 0
			wait for 5 ns;
			assert false report "130;0xff97b293;SLTIU : reg[05] = ( reg[15] < -7 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000254" report "progcounter error at step 260" severity error;
			wait for 5 ns;

		-- execute instruction 130
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 261" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 261" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 261" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 261" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 261" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 261" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 261" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 261" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 261" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 261" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 261" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 261" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 261" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 261" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 261" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 261" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 261" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 261" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 261" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 261" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 261" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 261" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 261" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 261" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 261" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 261" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 261" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 261" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 261" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 261" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 261" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 261" severity error;
			assert progcounter = x"00000258" report "progcounter error at step 261" severity error;
			wait for 5 ns;

		-- load instruction 131
			ck <= '0';
			instr <= x"ffb7b293"; -- SLTIU : reg[05] = ( reg[15] < -5 ) ? 1 : 0
			wait for 5 ns;
			assert false report "131;0xffb7b293;SLTIU : reg[05] = ( reg[15] < -5 ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000258" report "progcounter error at step 262" severity error;
			wait for 5 ns;

		-- execute instruction 131
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 263" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 263" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 263" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 263" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 263" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 263" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 263" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 263" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 263" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 263" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 263" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 263" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 263" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 263" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 263" severity error;
			assert reg0f = x"fffffffa" report "reg0f error at step 263" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 263" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 263" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 263" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 263" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 263" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 263" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 263" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 263" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 263" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 263" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 263" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 263" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 263" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 263" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 263" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 263" severity error;
			assert progcounter = x"0000025c" report "progcounter error at step 263" severity error;
			wait for 5 ns;

		-- load instruction 132
			ck <= '0';
			instr <= x"00600793"; -- ADDI : reg[15] = reg[00] + 6
			wait for 5 ns;
			assert false report "132;0x00600793;ADDI : reg[15] = reg[00] + 6;OK; ;" severity note;
			assert progcounter = x"0000025c" report "progcounter error at step 264" severity error;
			wait for 5 ns;

		-- execute instruction 132
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 265" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 265" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 265" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 265" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 265" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 265" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 265" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 265" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 265" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 265" severity error;
			assert reg0a = x"fffff000" report "reg0a error at step 265" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 265" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 265" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 265" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 265" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 265" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 265" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 265" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 265" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 265" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 265" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 265" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 265" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 265" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 265" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 265" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 265" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 265" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 265" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 265" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 265" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 265" severity error;
			assert progcounter = x"00000260" report "progcounter error at step 265" severity error;
			wait for 5 ns;

		-- load instruction 133
			ck <= '0';
			instr <= x"fff00513"; -- ADDI : reg[10] = reg[00] + -1
			wait for 5 ns;
			assert false report "133;0xfff00513;ADDI : reg[10] = reg[00] + -1;OK; ;" severity note;
			assert progcounter = x"00000260" report "progcounter error at step 266" severity error;
			wait for 5 ns;

		-- execute instruction 133
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 267" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 267" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 267" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 267" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 267" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 267" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 267" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 267" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 267" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 267" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 267" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 267" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 267" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 267" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 267" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 267" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 267" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 267" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 267" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 267" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 267" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 267" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 267" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 267" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 267" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 267" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 267" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 267" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 267" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 267" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 267" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 267" severity error;
			assert progcounter = x"00000264" report "progcounter error at step 267" severity error;
			wait for 5 ns;

		-- load instruction 134
			ck <= '0';
			instr <= x"55554513"; -- XORI: reg[10] = reg[10] ^ 1365
			wait for 5 ns;
			assert false report "134;0x55554513;XORI: reg[10] = reg[10] ^ 1365;OK; ;" severity note;
			assert progcounter = x"00000264" report "progcounter error at step 268" severity error;
			wait for 5 ns;

		-- execute instruction 134
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 269" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 269" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 269" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 269" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 269" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 269" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 269" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 269" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 269" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 269" severity error;
			assert reg0a = x"fffffaaa" report "reg0a error at step 269" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 269" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 269" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 269" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 269" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 269" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 269" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 269" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 269" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 269" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 269" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 269" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 269" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 269" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 269" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 269" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 269" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 269" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 269" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 269" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 269" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 269" severity error;
			assert progcounter = x"00000268" report "progcounter error at step 269" severity error;
			wait for 5 ns;

		-- load instruction 135
			ck <= '0';
			instr <= x"55554513"; -- XORI: reg[10] = reg[10] ^ 1365
			wait for 5 ns;
			assert false report "135;0x55554513;XORI: reg[10] = reg[10] ^ 1365;OK; ;" severity note;
			assert progcounter = x"00000268" report "progcounter error at step 270" severity error;
			wait for 5 ns;

		-- execute instruction 135
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 271" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 271" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 271" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 271" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 271" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 271" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 271" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 271" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 271" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 271" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 271" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 271" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 271" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 271" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 271" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 271" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 271" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 271" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 271" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 271" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 271" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 271" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 271" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 271" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 271" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 271" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 271" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 271" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 271" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 271" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 271" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 271" severity error;
			assert progcounter = x"0000026c" report "progcounter error at step 271" severity error;
			wait for 5 ns;

		-- load instruction 136
			ck <= '0';
			instr <= x"f0f54513"; -- XORI: reg[10] = reg[10] ^ -241
			wait for 5 ns;
			assert false report "136;0xf0f54513;XORI: reg[10] = reg[10] ^ -241;OK; ;" severity note;
			assert progcounter = x"0000026c" report "progcounter error at step 272" severity error;
			wait for 5 ns;

		-- execute instruction 136
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 273" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 273" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 273" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 273" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 273" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 273" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 273" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 273" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 273" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 273" severity error;
			assert reg0a = x"000000f0" report "reg0a error at step 273" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 273" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 273" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 273" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 273" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 273" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 273" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 273" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 273" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 273" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 273" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 273" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 273" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 273" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 273" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 273" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 273" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 273" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 273" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 273" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 273" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 273" severity error;
			assert progcounter = x"00000270" report "progcounter error at step 273" severity error;
			wait for 5 ns;

		-- load instruction 137
			ck <= '0';
			instr <= x"00000513"; -- ADDI : reg[10] = reg[00] + 0
			wait for 5 ns;
			assert false report "137;0x00000513;ADDI : reg[10] = reg[00] + 0;OK; ;" severity note;
			assert progcounter = x"00000270" report "progcounter error at step 274" severity error;
			wait for 5 ns;

		-- execute instruction 137
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 275" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 275" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 275" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 275" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 275" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 275" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 275" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 275" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 275" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 275" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 275" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 275" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 275" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 275" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 275" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 275" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 275" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 275" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 275" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 275" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 275" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 275" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 275" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 275" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 275" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 275" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 275" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 275" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 275" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 275" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 275" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 275" severity error;
			assert progcounter = x"00000274" report "progcounter error at step 275" severity error;
			wait for 5 ns;

		-- load instruction 138
			ck <= '0';
			instr <= x"f0f56513"; -- ORI: reg[10] = reg[10] | -241
			wait for 5 ns;
			assert false report "138;0xf0f56513;ORI: reg[10] = reg[10] | -241;OK; ;" severity note;
			assert progcounter = x"00000274" report "progcounter error at step 276" severity error;
			wait for 5 ns;

		-- execute instruction 138
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 277" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 277" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 277" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 277" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 277" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 277" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 277" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 277" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 277" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 277" severity error;
			assert reg0a = x"ffffff0f" report "reg0a error at step 277" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 277" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 277" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 277" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 277" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 277" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 277" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 277" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 277" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 277" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 277" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 277" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 277" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 277" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 277" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 277" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 277" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 277" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 277" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 277" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 277" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 277" severity error;
			assert progcounter = x"00000278" report "progcounter error at step 277" severity error;
			wait for 5 ns;

		-- load instruction 139
			ck <= '0';
			instr <= x"7ff56513"; -- ORI: reg[10] = reg[10] | 2047
			wait for 5 ns;
			assert false report "139;0x7ff56513;ORI: reg[10] = reg[10] | 2047;OK; ;" severity note;
			assert progcounter = x"00000278" report "progcounter error at step 278" severity error;
			wait for 5 ns;

		-- execute instruction 139
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 279" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 279" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 279" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 279" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 279" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 279" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 279" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 279" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 279" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 279" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 279" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 279" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 279" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 279" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 279" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 279" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 279" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 279" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 279" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 279" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 279" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 279" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 279" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 279" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 279" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 279" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 279" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 279" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 279" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 279" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 279" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 279" severity error;
			assert progcounter = x"0000027c" report "progcounter error at step 279" severity error;
			wait for 5 ns;

		-- load instruction 140
			ck <= '0';
			instr <= x"0ff57513"; -- ANDI: reg[10] = reg[10] & 255
			wait for 5 ns;
			assert false report "140;0x0ff57513;ANDI: reg[10] = reg[10] & 255;OK; ;" severity note;
			assert progcounter = x"0000027c" report "progcounter error at step 280" severity error;
			wait for 5 ns;

		-- execute instruction 140
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 281" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 281" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 281" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 281" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 281" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 281" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 281" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 281" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 281" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 281" severity error;
			assert reg0a = x"000000ff" report "reg0a error at step 281" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 281" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 281" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 281" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 281" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 281" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 281" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 281" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 281" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 281" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 281" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 281" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 281" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 281" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 281" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 281" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 281" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 281" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 281" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 281" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 281" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 281" severity error;
			assert progcounter = x"00000280" report "progcounter error at step 281" severity error;
			wait for 5 ns;

		-- load instruction 141
			ck <= '0';
			instr <= x"f0f57513"; -- ANDI: reg[10] = reg[10] & -241
			wait for 5 ns;
			assert false report "141;0xf0f57513;ANDI: reg[10] = reg[10] & -241;OK; ;" severity note;
			assert progcounter = x"00000280" report "progcounter error at step 282" severity error;
			wait for 5 ns;

		-- execute instruction 141
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 283" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 283" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 283" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 283" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 283" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 283" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 283" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 283" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 283" severity error;
			assert reg09 = x"fffff000" report "reg09 error at step 283" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 283" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 283" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 283" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 283" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 283" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 283" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 283" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 283" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 283" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 283" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 283" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 283" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 283" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 283" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 283" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 283" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 283" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 283" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 283" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 283" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 283" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 283" severity error;
			assert progcounter = x"00000284" report "progcounter error at step 283" severity error;
			wait for 5 ns;

		-- load instruction 142
			ck <= '0';
			instr <= x"00100493"; -- ADDI : reg[09] = reg[00] + 1
			wait for 5 ns;
			assert false report "142;0x00100493;ADDI : reg[09] = reg[00] + 1;OK; ;" severity note;
			assert progcounter = x"00000284" report "progcounter error at step 284" severity error;
			wait for 5 ns;

		-- execute instruction 142
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 285" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 285" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 285" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 285" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 285" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 285" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 285" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 285" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 285" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 285" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 285" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 285" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 285" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 285" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 285" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 285" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 285" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 285" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 285" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 285" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 285" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 285" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 285" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 285" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 285" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 285" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 285" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 285" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 285" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 285" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 285" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 285" severity error;
			assert progcounter = x"00000288" report "progcounter error at step 285" severity error;
			wait for 5 ns;

		-- load instruction 143
			ck <= '0';
			instr <= x"01f49613"; -- SLLI : reg[12] = reg[09] << 31 & 0x1f
			wait for 5 ns;
			assert false report "143;0x01f49613;SLLI : reg[12] = reg[09] << 31 & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000288" report "progcounter error at step 286" severity error;
			wait for 5 ns;

		-- execute instruction 143
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 287" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 287" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 287" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 287" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 287" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 287" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 287" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 287" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 287" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 287" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 287" severity error;
			assert reg0b = x"fffff000" report "reg0b error at step 287" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 287" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 287" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 287" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 287" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 287" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 287" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 287" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 287" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 287" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 287" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 287" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 287" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 287" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 287" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 287" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 287" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 287" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 287" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 287" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 287" severity error;
			assert progcounter = x"0000028c" report "progcounter error at step 287" severity error;
			wait for 5 ns;

		-- load instruction 144
			ck <= '0';
			instr <= x"00849593"; -- SLLI : reg[11] = reg[09] << 8 & 0x1f
			wait for 5 ns;
			assert false report "144;0x00849593;SLLI : reg[11] = reg[09] << 8 & 0x1f;OK; ;" severity note;
			assert progcounter = x"0000028c" report "progcounter error at step 288" severity error;
			wait for 5 ns;

		-- execute instruction 144
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 289" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 289" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 289" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 289" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 289" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 289" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 289" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 289" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 289" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 289" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 289" severity error;
			assert reg0b = x"00000100" report "reg0b error at step 289" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 289" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 289" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 289" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 289" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 289" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 289" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 289" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 289" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 289" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 289" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 289" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 289" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 289" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 289" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 289" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 289" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 289" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 289" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 289" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 289" severity error;
			assert progcounter = x"00000290" report "progcounter error at step 289" severity error;
			wait for 5 ns;

		-- load instruction 145
			ck <= '0';
			instr <= x"01059593"; -- SLLI : reg[11] = reg[11] << 16 & 0x1f
			wait for 5 ns;
			assert false report "145;0x01059593;SLLI : reg[11] = reg[11] << 16 & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000290" report "progcounter error at step 290" severity error;
			wait for 5 ns;

		-- execute instruction 145
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 291" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 291" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 291" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 291" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 291" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 291" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 291" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 291" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 291" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 291" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 291" severity error;
			assert reg0b = x"01000000" report "reg0b error at step 291" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 291" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 291" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 291" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 291" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 291" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 291" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 291" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 291" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 291" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 291" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 291" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 291" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 291" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 291" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 291" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 291" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 291" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 291" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 291" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 291" severity error;
			assert progcounter = x"00000294" report "progcounter error at step 291" severity error;
			wait for 5 ns;

		-- load instruction 146
			ck <= '0';
			instr <= x"fff00593"; -- ADDI : reg[11] = reg[00] + -1
			wait for 5 ns;
			assert false report "146;0xfff00593;ADDI : reg[11] = reg[00] + -1;OK; ;" severity note;
			assert progcounter = x"00000294" report "progcounter error at step 292" severity error;
			wait for 5 ns;

		-- execute instruction 146
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 293" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 293" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 293" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 293" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 293" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 293" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 293" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 293" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 293" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 293" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 293" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 293" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 293" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 293" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 293" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 293" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 293" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 293" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 293" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 293" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 293" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 293" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 293" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 293" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 293" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 293" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 293" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 293" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 293" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 293" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 293" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 293" severity error;
			assert progcounter = x"00000298" report "progcounter error at step 293" severity error;
			wait for 5 ns;

		-- load instruction 147
			ck <= '0';
			instr <= x"01f5d513"; -- SRLI : reg[10] = reg[11] >> 31 & 0x1f
			wait for 5 ns;
			assert false report "147;0x01f5d513;SRLI : reg[10] = reg[11] >> 31 & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000298" report "progcounter error at step 294" severity error;
			wait for 5 ns;

		-- execute instruction 147
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 295" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 295" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 295" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 295" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 295" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 295" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 295" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 295" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 295" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 295" severity error;
			assert reg0a = x"00000001" report "reg0a error at step 295" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 295" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 295" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 295" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 295" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 295" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 295" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 295" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 295" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 295" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 295" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 295" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 295" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 295" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 295" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 295" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 295" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 295" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 295" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 295" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 295" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 295" severity error;
			assert progcounter = x"0000029c" report "progcounter error at step 295" severity error;
			wait for 5 ns;

		-- load instruction 148
			ck <= '0';
			instr <= x"0085d513"; -- SRLI : reg[10] = reg[11] >> 8 & 0x1f
			wait for 5 ns;
			assert false report "148;0x0085d513;SRLI : reg[10] = reg[11] >> 8 & 0x1f;OK; ;" severity note;
			assert progcounter = x"0000029c" report "progcounter error at step 296" severity error;
			wait for 5 ns;

		-- execute instruction 148
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 297" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 297" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 297" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 297" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 297" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 297" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 297" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 297" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 297" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 297" severity error;
			assert reg0a = x"00ffffff" report "reg0a error at step 297" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 297" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 297" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 297" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 297" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 297" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 297" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 297" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 297" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 297" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 297" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 297" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 297" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 297" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 297" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 297" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 297" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 297" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 297" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 297" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 297" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 297" severity error;
			assert progcounter = x"000002a0" report "progcounter error at step 297" severity error;
			wait for 5 ns;

		-- load instruction 149
			ck <= '0';
			instr <= x"01055513"; -- SRLI : reg[10] = reg[10] >> 16 & 0x1f
			wait for 5 ns;
			assert false report "149;0x01055513;SRLI : reg[10] = reg[10] >> 16 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002a0" report "progcounter error at step 298" severity error;
			wait for 5 ns;

		-- execute instruction 149
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 299" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 299" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 299" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 299" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 299" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 299" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 299" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 299" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 299" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 299" severity error;
			assert reg0a = x"000000ff" report "reg0a error at step 299" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 299" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 299" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 299" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 299" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 299" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 299" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 299" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 299" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 299" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 299" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 299" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 299" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 299" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 299" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 299" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 299" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 299" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 299" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 299" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 299" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 299" severity error;
			assert progcounter = x"000002a4" report "progcounter error at step 299" severity error;
			wait for 5 ns;

		-- load instruction 150
			ck <= '0';
			instr <= x"01855513"; -- SRLI : reg[10] = reg[10] >> 24 & 0x1f
			wait for 5 ns;
			assert false report "150;0x01855513;SRLI : reg[10] = reg[10] >> 24 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002a4" report "progcounter error at step 300" severity error;
			wait for 5 ns;

		-- execute instruction 150
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 301" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 301" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 301" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 301" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 301" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 301" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 301" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 301" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 301" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 301" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 301" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 301" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 301" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 301" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 301" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 301" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 301" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 301" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 301" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 301" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 301" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 301" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 301" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 301" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 301" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 301" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 301" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 301" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 301" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 301" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 301" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 301" severity error;
			assert progcounter = x"000002a8" report "progcounter error at step 301" severity error;
			wait for 5 ns;

		-- load instruction 151
			ck <= '0';
			instr <= x"41f65593"; -- SRAI : reg[11] = (int) reg[12] >> (int) 31 & 0x1f
			wait for 5 ns;
			assert false report "151;0x41f65593;SRAI : reg[11] = (int) reg[12] >> (int) 31 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002a8" report "progcounter error at step 302" severity error;
			wait for 5 ns;

		-- execute instruction 151
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 303" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 303" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 303" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 303" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 303" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 303" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 303" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 303" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 303" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 303" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 303" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 303" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 303" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 303" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 303" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 303" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 303" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 303" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 303" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 303" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 303" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 303" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 303" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 303" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 303" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 303" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 303" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 303" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 303" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 303" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 303" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 303" severity error;
			assert progcounter = x"000002ac" report "progcounter error at step 303" severity error;
			wait for 5 ns;

		-- load instruction 152
			ck <= '0';
			instr <= x"40865593"; -- SRAI : reg[11] = (int) reg[12] >> (int) 8 & 0x1f
			wait for 5 ns;
			assert false report "152;0x40865593;SRAI : reg[11] = (int) reg[12] >> (int) 8 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002ac" report "progcounter error at step 304" severity error;
			wait for 5 ns;

		-- execute instruction 152
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 305" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 305" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 305" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 305" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 305" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 305" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 305" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 305" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 305" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 305" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 305" severity error;
			assert reg0b = x"ff800000" report "reg0b error at step 305" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 305" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 305" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 305" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 305" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 305" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 305" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 305" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 305" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 305" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 305" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 305" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 305" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 305" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 305" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 305" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 305" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 305" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 305" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 305" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 305" severity error;
			assert progcounter = x"000002b0" report "progcounter error at step 305" severity error;
			wait for 5 ns;

		-- load instruction 153
			ck <= '0';
			instr <= x"4105d593"; -- SRAI : reg[11] = (int) reg[11] >> (int) 16 & 0x1f
			wait for 5 ns;
			assert false report "153;0x4105d593;SRAI : reg[11] = (int) reg[11] >> (int) 16 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002b0" report "progcounter error at step 306" severity error;
			wait for 5 ns;

		-- execute instruction 153
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 307" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 307" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 307" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 307" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 307" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 307" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 307" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 307" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 307" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 307" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 307" severity error;
			assert reg0b = x"ffffff80" report "reg0b error at step 307" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 307" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 307" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 307" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 307" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 307" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 307" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 307" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 307" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 307" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 307" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 307" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 307" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 307" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 307" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 307" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 307" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 307" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 307" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 307" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 307" severity error;
			assert progcounter = x"000002b4" report "progcounter error at step 307" severity error;
			wait for 5 ns;

		-- load instruction 154
			ck <= '0';
			instr <= x"4185d593"; -- SRAI : reg[11] = (int) reg[11] >> (int) 24 & 0x1f
			wait for 5 ns;
			assert false report "154;0x4185d593;SRAI : reg[11] = (int) reg[11] >> (int) 24 & 0x1f;OK; ;" severity note;
			assert progcounter = x"000002b4" report "progcounter error at step 308" severity error;
			wait for 5 ns;

		-- execute instruction 154
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 309" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 309" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 309" severity error;
			assert reg03 = x"000000c0" report "reg03 error at step 309" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 309" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 309" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 309" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 309" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 309" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 309" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 309" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 309" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 309" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 309" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 309" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 309" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 309" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 309" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 309" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 309" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 309" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 309" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 309" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 309" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 309" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 309" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 309" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 309" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 309" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 309" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 309" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 309" severity error;
			assert progcounter = x"000002b8" report "progcounter error at step 309" severity error;
			wait for 5 ns;

		-- load instruction 155
			ck <= '0';
			instr <= x"80000193"; -- ADDI : reg[03] = reg[00] + -2048
			wait for 5 ns;
			assert false report "155;0x80000193;ADDI : reg[03] = reg[00] + -2048;OK; ;" severity note;
			assert progcounter = x"000002b8" report "progcounter error at step 310" severity error;
			wait for 5 ns;

		-- execute instruction 155
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 311" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 311" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 311" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 311" severity error;
			assert reg04 = x"fffff000" report "reg04 error at step 311" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 311" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 311" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 311" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 311" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 311" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 311" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 311" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 311" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 311" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 311" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 311" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 311" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 311" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 311" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 311" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 311" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 311" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 311" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 311" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 311" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 311" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 311" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 311" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 311" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 311" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 311" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 311" severity error;
			assert progcounter = x"000002bc" report "progcounter error at step 311" severity error;
			wait for 5 ns;

		-- load instruction 156
			ck <= '0';
			instr <= x"7ff00213"; -- ADDI : reg[04] = reg[00] + 2047
			wait for 5 ns;
			assert false report "156;0x7ff00213;ADDI : reg[04] = reg[00] + 2047;OK; ;" severity note;
			assert progcounter = x"000002bc" report "progcounter error at step 312" severity error;
			wait for 5 ns;

		-- execute instruction 156
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 313" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 313" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 313" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 313" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 313" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 313" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 313" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 313" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 313" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 313" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 313" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 313" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 313" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 313" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 313" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 313" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 313" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 313" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 313" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 313" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 313" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 313" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 313" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 313" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 313" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 313" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 313" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 313" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 313" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 313" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 313" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 313" severity error;
			assert progcounter = x"000002c0" report "progcounter error at step 313" severity error;
			wait for 5 ns;

		-- load instruction 157
			ck <= '0';
			instr <= x"00300633"; -- ADD: reg[12] = reg[00] + reg[03]
			wait for 5 ns;
			assert false report "157;0x00300633;ADD: reg[12] = reg[00] + reg[03];OK; ;" severity note;
			assert progcounter = x"000002c0" report "progcounter error at step 314" severity error;
			wait for 5 ns;

		-- execute instruction 157
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 315" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 315" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 315" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 315" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 315" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 315" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 315" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 315" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 315" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 315" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 315" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 315" severity error;
			assert reg0c = x"fffff800" report "reg0c error at step 315" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 315" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 315" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 315" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 315" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 315" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 315" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 315" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 315" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 315" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 315" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 315" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 315" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 315" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 315" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 315" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 315" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 315" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 315" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 315" severity error;
			assert progcounter = x"000002c4" report "progcounter error at step 315" severity error;
			wait for 5 ns;

		-- load instruction 158
			ck <= '0';
			instr <= x"00460633"; -- ADD: reg[12] = reg[12] + reg[04]
			wait for 5 ns;
			assert false report "158;0x00460633;ADD: reg[12] = reg[12] + reg[04];OK; ;" severity note;
			assert progcounter = x"000002c4" report "progcounter error at step 316" severity error;
			wait for 5 ns;

		-- execute instruction 158
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 317" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 317" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 317" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 317" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 317" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 317" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 317" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 317" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 317" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 317" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 317" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 317" severity error;
			assert reg0c = x"ffffffff" report "reg0c error at step 317" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 317" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 317" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 317" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 317" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 317" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 317" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 317" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 317" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 317" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 317" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 317" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 317" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 317" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 317" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 317" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 317" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 317" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 317" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 317" severity error;
			assert progcounter = x"000002c8" report "progcounter error at step 317" severity error;
			wait for 5 ns;

		-- load instruction 159
			ck <= '0';
			instr <= x"00460633"; -- ADD: reg[12] = reg[12] + reg[04]
			wait for 5 ns;
			assert false report "159;0x00460633;ADD: reg[12] = reg[12] + reg[04];OK; ;" severity note;
			assert progcounter = x"000002c8" report "progcounter error at step 318" severity error;
			wait for 5 ns;

		-- execute instruction 159
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 319" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 319" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 319" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 319" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 319" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 319" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 319" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 319" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 319" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 319" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 319" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 319" severity error;
			assert reg0c = x"000007fe" report "reg0c error at step 319" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 319" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 319" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 319" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 319" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 319" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 319" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 319" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 319" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 319" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 319" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 319" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 319" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 319" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 319" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 319" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 319" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 319" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 319" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 319" severity error;
			assert progcounter = x"000002cc" report "progcounter error at step 319" severity error;
			wait for 5 ns;

		-- load instruction 160
			ck <= '0';
			instr <= x"00000633"; -- ADD: reg[12] = reg[00] + reg[00]
			wait for 5 ns;
			assert false report "160;0x00000633;ADD: reg[12] = reg[00] + reg[00];OK; ;" severity note;
			assert progcounter = x"000002cc" report "progcounter error at step 320" severity error;
			wait for 5 ns;

		-- execute instruction 160
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 321" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 321" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 321" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 321" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 321" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 321" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 321" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 321" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 321" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 321" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 321" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 321" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 321" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 321" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 321" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 321" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 321" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 321" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 321" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 321" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 321" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 321" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 321" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 321" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 321" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 321" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 321" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 321" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 321" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 321" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 321" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 321" severity error;
			assert progcounter = x"000002d0" report "progcounter error at step 321" severity error;
			wait for 5 ns;

		-- load instruction 161
			ck <= '0';
			instr <= x"00600793"; -- ADDI : reg[15] = reg[00] + 6
			wait for 5 ns;
			assert false report "161;0x00600793;ADDI : reg[15] = reg[00] + 6;OK; ;" severity note;
			assert progcounter = x"000002d0" report "progcounter error at step 322" severity error;
			wait for 5 ns;

		-- execute instruction 161
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 323" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 323" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 323" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 323" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 323" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 323" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 323" severity error;
			assert reg07 = x"fffff000" report "reg07 error at step 323" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 323" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 323" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 323" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 323" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 323" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 323" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 323" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 323" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 323" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 323" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 323" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 323" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 323" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 323" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 323" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 323" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 323" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 323" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 323" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 323" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 323" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 323" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 323" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 323" severity error;
			assert progcounter = x"000002d4" report "progcounter error at step 323" severity error;
			wait for 5 ns;

		-- load instruction 162
			ck <= '0';
			instr <= x"ff900393"; -- ADDI : reg[07] = reg[00] + -7
			wait for 5 ns;
			assert false report "162;0xff900393;ADDI : reg[07] = reg[00] + -7;OK; ;" severity note;
			assert progcounter = x"000002d4" report "progcounter error at step 324" severity error;
			wait for 5 ns;

		-- execute instruction 162
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 325" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 325" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 325" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 325" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 325" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 325" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 325" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 325" severity error;
			assert reg08 = x"fffff000" report "reg08 error at step 325" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 325" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 325" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 325" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 325" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 325" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 325" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 325" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 325" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 325" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 325" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 325" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 325" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 325" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 325" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 325" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 325" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 325" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 325" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 325" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 325" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 325" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 325" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 325" severity error;
			assert progcounter = x"000002d8" report "progcounter error at step 325" severity error;
			wait for 5 ns;

		-- load instruction 163
			ck <= '0';
			instr <= x"00700413"; -- ADDI : reg[08] = reg[00] + 7
			wait for 5 ns;
			assert false report "163;0x00700413;ADDI : reg[08] = reg[00] + 7;OK; ;" severity note;
			assert progcounter = x"000002d8" report "progcounter error at step 326" severity error;
			wait for 5 ns;

		-- execute instruction 163
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 327" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 327" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 327" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 327" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 327" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 327" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 327" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 327" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 327" severity error;
			assert reg09 = x"00000001" report "reg09 error at step 327" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 327" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 327" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 327" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 327" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 327" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 327" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 327" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 327" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 327" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 327" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 327" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 327" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 327" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 327" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 327" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 327" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 327" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 327" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 327" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 327" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 327" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 327" severity error;
			assert progcounter = x"000002dc" report "progcounter error at step 327" severity error;
			wait for 5 ns;

		-- load instruction 164
			ck <= '0';
			instr <= x"ffa00493"; -- ADDI : reg[09] = reg[00] + -6
			wait for 5 ns;
			assert false report "164;0xffa00493;ADDI : reg[09] = reg[00] + -6;OK; ;" severity note;
			assert progcounter = x"000002dc" report "progcounter error at step 328" severity error;
			wait for 5 ns;

		-- execute instruction 164
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 329" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 329" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 329" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 329" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 329" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 329" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 329" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 329" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 329" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 329" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 329" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 329" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 329" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 329" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 329" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 329" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 329" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 329" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 329" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 329" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 329" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 329" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 329" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 329" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 329" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 329" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 329" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 329" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 329" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 329" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 329" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 329" severity error;
			assert progcounter = x"000002e0" report "progcounter error at step 329" severity error;
			wait for 5 ns;

		-- load instruction 165
			ck <= '0';
			instr <= x"ffb00513"; -- ADDI : reg[10] = reg[00] + -5
			wait for 5 ns;
			assert false report "165;0xffb00513;ADDI : reg[10] = reg[00] + -5;OK; ;" severity note;
			assert progcounter = x"000002e0" report "progcounter error at step 330" severity error;
			wait for 5 ns;

		-- execute instruction 165
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 331" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 331" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 331" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 331" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 331" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 331" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 331" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 331" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 331" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 331" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 331" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 331" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 331" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 331" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 331" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 331" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 331" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 331" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 331" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 331" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 331" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 331" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 331" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 331" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 331" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 331" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 331" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 331" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 331" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 331" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 331" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 331" severity error;
			assert progcounter = x"000002e4" report "progcounter error at step 331" severity error;
			wait for 5 ns;

		-- load instruction 166
			ck <= '0';
			instr <= x"00f7a2b3"; -- SLT : reg[05] = ( reg[15] < reg[15] ) ? 1 : 0
			wait for 5 ns;
			assert false report "166;0x00f7a2b3;SLT : reg[05] = ( reg[15] < reg[15] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002e4" report "progcounter error at step 332" severity error;
			wait for 5 ns;

		-- execute instruction 166
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 333" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 333" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 333" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 333" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 333" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 333" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 333" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 333" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 333" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 333" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 333" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 333" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 333" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 333" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 333" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 333" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 333" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 333" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 333" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 333" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 333" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 333" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 333" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 333" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 333" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 333" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 333" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 333" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 333" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 333" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 333" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 333" severity error;
			assert progcounter = x"000002e8" report "progcounter error at step 333" severity error;
			wait for 5 ns;

		-- load instruction 167
			ck <= '0';
			instr <= x"0077a2b3"; -- SLT : reg[05] = ( reg[15] < reg[07] ) ? 1 : 0
			wait for 5 ns;
			assert false report "167;0x0077a2b3;SLT : reg[05] = ( reg[15] < reg[07] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002e8" report "progcounter error at step 334" severity error;
			wait for 5 ns;

		-- execute instruction 167
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 335" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 335" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 335" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 335" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 335" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 335" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 335" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 335" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 335" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 335" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 335" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 335" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 335" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 335" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 335" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 335" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 335" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 335" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 335" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 335" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 335" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 335" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 335" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 335" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 335" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 335" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 335" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 335" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 335" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 335" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 335" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 335" severity error;
			assert progcounter = x"000002ec" report "progcounter error at step 335" severity error;
			wait for 5 ns;

		-- load instruction 168
			ck <= '0';
			instr <= x"0087a2b3"; -- SLT : reg[05] = ( reg[15] < reg[08] ) ? 1 : 0
			wait for 5 ns;
			assert false report "168;0x0087a2b3;SLT : reg[05] = ( reg[15] < reg[08] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002ec" report "progcounter error at step 336" severity error;
			wait for 5 ns;

		-- execute instruction 168
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 337" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 337" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 337" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 337" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 337" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 337" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 337" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 337" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 337" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 337" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 337" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 337" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 337" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 337" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 337" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 337" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 337" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 337" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 337" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 337" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 337" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 337" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 337" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 337" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 337" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 337" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 337" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 337" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 337" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 337" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 337" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 337" severity error;
			assert progcounter = x"000002f0" report "progcounter error at step 337" severity error;
			wait for 5 ns;

		-- load instruction 169
			ck <= '0';
			instr <= x"0074a2b3"; -- SLT : reg[05] = ( reg[09] < reg[07] ) ? 1 : 0
			wait for 5 ns;
			assert false report "169;0x0074a2b3;SLT : reg[05] = ( reg[09] < reg[07] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002f0" report "progcounter error at step 338" severity error;
			wait for 5 ns;

		-- execute instruction 169
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 339" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 339" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 339" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 339" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 339" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 339" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 339" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 339" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 339" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 339" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 339" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 339" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 339" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 339" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 339" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 339" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 339" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 339" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 339" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 339" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 339" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 339" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 339" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 339" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 339" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 339" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 339" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 339" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 339" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 339" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 339" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 339" severity error;
			assert progcounter = x"000002f4" report "progcounter error at step 339" severity error;
			wait for 5 ns;

		-- load instruction 170
			ck <= '0';
			instr <= x"0004a2b3"; -- SLT : reg[05] = ( reg[09] < reg[00] ) ? 1 : 0
			wait for 5 ns;
			assert false report "170;0x0004a2b3;SLT : reg[05] = ( reg[09] < reg[00] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002f4" report "progcounter error at step 340" severity error;
			wait for 5 ns;

		-- execute instruction 170
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 341" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 341" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 341" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 341" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 341" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 341" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 341" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 341" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 341" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 341" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 341" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 341" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 341" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 341" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 341" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 341" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 341" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 341" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 341" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 341" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 341" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 341" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 341" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 341" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 341" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 341" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 341" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 341" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 341" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 341" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 341" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 341" severity error;
			assert progcounter = x"000002f8" report "progcounter error at step 341" severity error;
			wait for 5 ns;

		-- load instruction 171
			ck <= '0';
			instr <= x"00f4b2b3"; -- SLTU: reg[05] = ( reg[09] < reg[15] ) ? 1 : 0
			wait for 5 ns;
			assert false report "171;0x00f4b2b3;SLTU: reg[05] = ( reg[09] < reg[15] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002f8" report "progcounter error at step 342" severity error;
			wait for 5 ns;

		-- execute instruction 171
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 343" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 343" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 343" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 343" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 343" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 343" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 343" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 343" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 343" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 343" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 343" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 343" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 343" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 343" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 343" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 343" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 343" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 343" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 343" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 343" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 343" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 343" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 343" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 343" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 343" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 343" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 343" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 343" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 343" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 343" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 343" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 343" severity error;
			assert progcounter = x"000002fc" report "progcounter error at step 343" severity error;
			wait for 5 ns;

		-- load instruction 172
			ck <= '0';
			instr <= x"0074b2b3"; -- SLTU: reg[05] = ( reg[09] < reg[07] ) ? 1 : 0
			wait for 5 ns;
			assert false report "172;0x0074b2b3;SLTU: reg[05] = ( reg[09] < reg[07] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"000002fc" report "progcounter error at step 344" severity error;
			wait for 5 ns;

		-- execute instruction 172
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 345" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 345" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 345" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 345" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 345" severity error;
			assert reg05 = x"00000000" report "reg05 error at step 345" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 345" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 345" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 345" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 345" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 345" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 345" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 345" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 345" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 345" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 345" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 345" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 345" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 345" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 345" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 345" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 345" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 345" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 345" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 345" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 345" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 345" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 345" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 345" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 345" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 345" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 345" severity error;
			assert progcounter = x"00000300" report "progcounter error at step 345" severity error;
			wait for 5 ns;

		-- load instruction 173
			ck <= '0';
			instr <= x"00a4b2b3"; -- SLTU: reg[05] = ( reg[09] < reg[10] ) ? 1 : 0
			wait for 5 ns;
			assert false report "173;0x00a4b2b3;SLTU: reg[05] = ( reg[09] < reg[10] ) ? 1 : 0;OK; ;" severity note;
			assert progcounter = x"00000300" report "progcounter error at step 346" severity error;
			wait for 5 ns;

		-- execute instruction 173
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 347" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 347" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 347" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 347" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 347" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 347" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 347" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 347" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 347" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 347" severity error;
			assert reg0a = x"fffffffb" report "reg0a error at step 347" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 347" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 347" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 347" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 347" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 347" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 347" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 347" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 347" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 347" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 347" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 347" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 347" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 347" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 347" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 347" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 347" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 347" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 347" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 347" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 347" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 347" severity error;
			assert progcounter = x"00000304" report "progcounter error at step 347" severity error;
			wait for 5 ns;

		-- load instruction 174
			ck <= '0';
			instr <= x"fff00513"; -- ADDI : reg[10] = reg[00] + -1
			wait for 5 ns;
			assert false report "174;0xfff00513;ADDI : reg[10] = reg[00] + -1;OK; ;" severity note;
			assert progcounter = x"00000304" report "progcounter error at step 348" severity error;
			wait for 5 ns;

		-- execute instruction 174
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 349" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 349" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 349" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 349" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 349" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 349" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 349" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 349" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 349" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 349" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 349" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 349" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 349" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 349" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 349" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 349" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 349" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 349" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 349" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 349" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 349" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 349" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 349" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 349" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 349" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 349" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 349" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 349" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 349" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 349" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 349" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 349" severity error;
			assert progcounter = x"00000308" report "progcounter error at step 349" severity error;
			wait for 5 ns;

		-- load instruction 175
			ck <= '0';
			instr <= x"55500593"; -- ADDI : reg[11] = reg[00] + 1365
			wait for 5 ns;
			assert false report "175;0x55500593;ADDI : reg[11] = reg[00] + 1365;OK; ;" severity note;
			assert progcounter = x"00000308" report "progcounter error at step 350" severity error;
			wait for 5 ns;

		-- execute instruction 175
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 351" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 351" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 351" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 351" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 351" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 351" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 351" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 351" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 351" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 351" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 351" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 351" severity error;
			assert reg0c = x"00000000" report "reg0c error at step 351" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 351" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 351" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 351" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 351" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 351" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 351" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 351" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 351" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 351" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 351" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 351" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 351" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 351" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 351" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 351" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 351" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 351" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 351" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 351" severity error;
			assert progcounter = x"0000030c" report "progcounter error at step 351" severity error;
			wait for 5 ns;

		-- load instruction 176
			ck <= '0';
			instr <= x"f0f00613"; -- ADDI : reg[12] = reg[00] + -241
			wait for 5 ns;
			assert false report "176;0xf0f00613;ADDI : reg[12] = reg[00] + -241;OK; ;" severity note;
			assert progcounter = x"0000030c" report "progcounter error at step 352" severity error;
			wait for 5 ns;

		-- execute instruction 176
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 353" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 353" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 353" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 353" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 353" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 353" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 353" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 353" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 353" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 353" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 353" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 353" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 353" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 353" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 353" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 353" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 353" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 353" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 353" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 353" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 353" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 353" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 353" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 353" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 353" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 353" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 353" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 353" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 353" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 353" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 353" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 353" severity error;
			assert progcounter = x"00000310" report "progcounter error at step 353" severity error;
			wait for 5 ns;

		-- load instruction 177
			ck <= '0';
			instr <= x"00b54533"; -- XOR: reg[10] = reg[10] ^ reg[11]
			wait for 5 ns;
			assert false report "177;0x00b54533;XOR: reg[10] = reg[10] ^ reg[11];OK; ;" severity note;
			assert progcounter = x"00000310" report "progcounter error at step 354" severity error;
			wait for 5 ns;

		-- execute instruction 177
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 355" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 355" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 355" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 355" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 355" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 355" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 355" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 355" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 355" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 355" severity error;
			assert reg0a = x"fffffaaa" report "reg0a error at step 355" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 355" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 355" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 355" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 355" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 355" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 355" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 355" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 355" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 355" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 355" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 355" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 355" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 355" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 355" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 355" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 355" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 355" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 355" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 355" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 355" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 355" severity error;
			assert progcounter = x"00000314" report "progcounter error at step 355" severity error;
			wait for 5 ns;

		-- load instruction 178
			ck <= '0';
			instr <= x"00b54533"; -- XOR: reg[10] = reg[10] ^ reg[11]
			wait for 5 ns;
			assert false report "178;0x00b54533;XOR: reg[10] = reg[10] ^ reg[11];OK; ;" severity note;
			assert progcounter = x"00000314" report "progcounter error at step 356" severity error;
			wait for 5 ns;

		-- execute instruction 178
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 357" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 357" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 357" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 357" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 357" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 357" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 357" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 357" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 357" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 357" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 357" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 357" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 357" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 357" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 357" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 357" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 357" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 357" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 357" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 357" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 357" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 357" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 357" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 357" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 357" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 357" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 357" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 357" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 357" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 357" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 357" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 357" severity error;
			assert progcounter = x"00000318" report "progcounter error at step 357" severity error;
			wait for 5 ns;

		-- load instruction 179
			ck <= '0';
			instr <= x"00c54533"; -- XOR: reg[10] = reg[10] ^ reg[12]
			wait for 5 ns;
			assert false report "179;0x00c54533;XOR: reg[10] = reg[10] ^ reg[12];OK; ;" severity note;
			assert progcounter = x"00000318" report "progcounter error at step 358" severity error;
			wait for 5 ns;

		-- execute instruction 179
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 359" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 359" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 359" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 359" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 359" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 359" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 359" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 359" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 359" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 359" severity error;
			assert reg0a = x"000000f0" report "reg0a error at step 359" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 359" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 359" severity error;
			assert reg0d = x"00000004" report "reg0d error at step 359" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 359" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 359" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 359" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 359" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 359" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 359" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 359" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 359" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 359" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 359" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 359" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 359" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 359" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 359" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 359" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 359" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 359" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 359" severity error;
			assert progcounter = x"0000031c" report "progcounter error at step 359" severity error;
			wait for 5 ns;

		-- load instruction 180
			ck <= '0';
			instr <= x"7ff00693"; -- ADDI : reg[13] = reg[00] + 2047
			wait for 5 ns;
			assert false report "180;0x7ff00693;ADDI : reg[13] = reg[00] + 2047;OK; ;" severity note;
			assert progcounter = x"0000031c" report "progcounter error at step 360" severity error;
			wait for 5 ns;

		-- execute instruction 180
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 361" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 361" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 361" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 361" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 361" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 361" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 361" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 361" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 361" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 361" severity error;
			assert reg0a = x"000000f0" report "reg0a error at step 361" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 361" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 361" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 361" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 361" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 361" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 361" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 361" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 361" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 361" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 361" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 361" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 361" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 361" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 361" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 361" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 361" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 361" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 361" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 361" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 361" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 361" severity error;
			assert progcounter = x"00000320" report "progcounter error at step 361" severity error;
			wait for 5 ns;

		-- load instruction 181
			ck <= '0';
			instr <= x"00c06533"; -- OR: reg[10] = reg[00] | reg[12]
			wait for 5 ns;
			assert false report "181;0x00c06533;OR: reg[10] = reg[00] | reg[12];OK; ;" severity note;
			assert progcounter = x"00000320" report "progcounter error at step 362" severity error;
			wait for 5 ns;

		-- execute instruction 181
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 363" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 363" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 363" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 363" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 363" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 363" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 363" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 363" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 363" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 363" severity error;
			assert reg0a = x"ffffff0f" report "reg0a error at step 363" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 363" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 363" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 363" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 363" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 363" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 363" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 363" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 363" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 363" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 363" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 363" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 363" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 363" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 363" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 363" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 363" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 363" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 363" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 363" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 363" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 363" severity error;
			assert progcounter = x"00000324" report "progcounter error at step 363" severity error;
			wait for 5 ns;

		-- load instruction 182
			ck <= '0';
			instr <= x"00d56533"; -- OR: reg[10] = reg[10] | reg[13]
			wait for 5 ns;
			assert false report "182;0x00d56533;OR: reg[10] = reg[10] | reg[13];OK; ;" severity note;
			assert progcounter = x"00000324" report "progcounter error at step 364" severity error;
			wait for 5 ns;

		-- execute instruction 182
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 365" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 365" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 365" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 365" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 365" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 365" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 365" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 365" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 365" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 365" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 365" severity error;
			assert reg0b = x"00000555" report "reg0b error at step 365" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 365" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 365" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 365" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 365" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 365" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 365" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 365" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 365" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 365" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 365" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 365" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 365" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 365" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 365" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 365" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 365" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 365" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 365" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 365" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 365" severity error;
			assert progcounter = x"00000328" report "progcounter error at step 365" severity error;
			wait for 5 ns;

		-- load instruction 183
			ck <= '0';
			instr <= x"0ff00593"; -- ADDI : reg[11] = reg[00] + 255
			wait for 5 ns;
			assert false report "183;0x0ff00593;ADDI : reg[11] = reg[00] + 255;OK; ;" severity note;
			assert progcounter = x"00000328" report "progcounter error at step 366" severity error;
			wait for 5 ns;

		-- execute instruction 183
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 367" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 367" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 367" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 367" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 367" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 367" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 367" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 367" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 367" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 367" severity error;
			assert reg0a = x"ffffffff" report "reg0a error at step 367" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 367" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 367" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 367" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 367" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 367" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 367" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 367" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 367" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 367" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 367" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 367" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 367" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 367" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 367" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 367" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 367" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 367" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 367" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 367" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 367" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 367" severity error;
			assert progcounter = x"0000032c" report "progcounter error at step 367" severity error;
			wait for 5 ns;

		-- load instruction 184
			ck <= '0';
			instr <= x"00b57533"; -- AND: reg[10] = reg[10] & reg[11]
			wait for 5 ns;
			assert false report "184;0x00b57533;AND: reg[10] = reg[10] & reg[11];OK; ;" severity note;
			assert progcounter = x"0000032c" report "progcounter error at step 368" severity error;
			wait for 5 ns;

		-- execute instruction 184
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 369" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 369" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 369" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 369" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 369" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 369" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 369" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 369" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 369" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 369" severity error;
			assert reg0a = x"000000ff" report "reg0a error at step 369" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 369" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 369" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 369" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 369" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 369" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 369" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 369" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 369" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 369" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 369" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 369" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 369" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 369" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 369" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 369" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 369" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 369" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 369" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 369" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 369" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 369" severity error;
			assert progcounter = x"00000330" report "progcounter error at step 369" severity error;
			wait for 5 ns;

		-- load instruction 185
			ck <= '0';
			instr <= x"00c57533"; -- AND: reg[10] = reg[10] & reg[12]
			wait for 5 ns;
			assert false report "185;0x00c57533;AND: reg[10] = reg[10] & reg[12];OK; ;" severity note;
			assert progcounter = x"00000330" report "progcounter error at step 370" severity error;
			wait for 5 ns;

		-- execute instruction 185
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 371" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 371" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 371" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 371" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 371" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 371" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 371" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 371" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 371" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 371" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 371" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 371" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 371" severity error;
			assert reg0d = x"000007ff" report "reg0d error at step 371" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 371" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 371" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 371" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 371" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 371" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 371" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 371" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 371" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 371" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 371" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 371" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 371" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 371" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 371" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 371" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 371" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 371" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 371" severity error;
			assert progcounter = x"00000334" report "progcounter error at step 371" severity error;
			wait for 5 ns;

		-- load instruction 186
			ck <= '0';
			instr <= x"00100693"; -- ADDI : reg[13] = reg[00] + 1
			wait for 5 ns;
			assert false report "186;0x00100693;ADDI : reg[13] = reg[00] + 1;OK; ;" severity note;
			assert progcounter = x"00000334" report "progcounter error at step 372" severity error;
			wait for 5 ns;

		-- execute instruction 186
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 373" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 373" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 373" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 373" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 373" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 373" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 373" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 373" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 373" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 373" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 373" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 373" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 373" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 373" severity error;
			assert reg0e = x"00000004" report "reg0e error at step 373" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 373" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 373" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 373" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 373" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 373" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 373" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 373" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 373" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 373" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 373" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 373" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 373" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 373" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 373" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 373" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 373" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 373" severity error;
			assert progcounter = x"00000338" report "progcounter error at step 373" severity error;
			wait for 5 ns;

		-- load instruction 187
			ck <= '0';
			instr <= x"01f00713"; -- ADDI : reg[14] = reg[00] + 31
			wait for 5 ns;
			assert false report "187;0x01f00713;ADDI : reg[14] = reg[00] + 31;OK; ;" severity note;
			assert progcounter = x"00000338" report "progcounter error at step 374" severity error;
			wait for 5 ns;

		-- execute instruction 187
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 375" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 375" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 375" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 375" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 375" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 375" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 375" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 375" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 375" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 375" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 375" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 375" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 375" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 375" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 375" severity error;
			assert reg0f = x"00000006" report "reg0f error at step 375" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 375" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 375" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 375" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 375" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 375" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 375" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 375" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 375" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 375" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 375" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 375" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 375" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 375" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 375" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 375" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 375" severity error;
			assert progcounter = x"0000033c" report "progcounter error at step 375" severity error;
			wait for 5 ns;

		-- load instruction 188
			ck <= '0';
			instr <= x"00800793"; -- ADDI : reg[15] = reg[00] + 8
			wait for 5 ns;
			assert false report "188;0x00800793;ADDI : reg[15] = reg[00] + 8;OK; ;" severity note;
			assert progcounter = x"0000033c" report "progcounter error at step 376" severity error;
			wait for 5 ns;

		-- execute instruction 188
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 377" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 377" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 377" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 377" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 377" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 377" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 377" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 377" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 377" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 377" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 377" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 377" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 377" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 377" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 377" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 377" severity error;
			assert reg10 = x"fffffffc" report "reg10 error at step 377" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 377" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 377" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 377" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 377" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 377" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 377" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 377" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 377" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 377" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 377" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 377" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 377" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 377" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 377" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 377" severity error;
			assert progcounter = x"00000340" report "progcounter error at step 377" severity error;
			wait for 5 ns;

		-- load instruction 189
			ck <= '0';
			instr <= x"01000813"; -- ADDI : reg[16] = reg[00] + 16
			wait for 5 ns;
			assert false report "189;0x01000813;ADDI : reg[16] = reg[00] + 16;OK; ;" severity note;
			assert progcounter = x"00000340" report "progcounter error at step 378" severity error;
			wait for 5 ns;

		-- execute instruction 189
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 379" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 379" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 379" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 379" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 379" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 379" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 379" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 379" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 379" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 379" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 379" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 379" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 379" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 379" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 379" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 379" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 379" severity error;
			assert reg11 = x"fffffffc" report "reg11 error at step 379" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 379" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 379" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 379" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 379" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 379" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 379" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 379" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 379" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 379" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 379" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 379" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 379" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 379" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 379" severity error;
			assert progcounter = x"00000344" report "progcounter error at step 379" severity error;
			wait for 5 ns;

		-- load instruction 190
			ck <= '0';
			instr <= x"01800893"; -- ADDI : reg[17] = reg[00] + 24
			wait for 5 ns;
			assert false report "190;0x01800893;ADDI : reg[17] = reg[00] + 24;OK; ;" severity note;
			assert progcounter = x"00000344" report "progcounter error at step 380" severity error;
			wait for 5 ns;

		-- execute instruction 190
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 381" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 381" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 381" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 381" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 381" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 381" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 381" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 381" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 381" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 381" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 381" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 381" severity error;
			assert reg0c = x"ffffff0f" report "reg0c error at step 381" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 381" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 381" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 381" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 381" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 381" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 381" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 381" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 381" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 381" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 381" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 381" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 381" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 381" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 381" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 381" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 381" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 381" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 381" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 381" severity error;
			assert progcounter = x"00000348" report "progcounter error at step 381" severity error;
			wait for 5 ns;

		-- load instruction 191
			ck <= '0';
			instr <= x"00e69633"; -- SLL : reg[12] = reg[13] << reg[14] & 0x1f
			wait for 5 ns;
			assert false report "191;0x00e69633;SLL : reg[12] = reg[13] << reg[14] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000348" report "progcounter error at step 382" severity error;
			wait for 5 ns;

		-- execute instruction 191
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 383" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 383" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 383" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 383" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 383" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 383" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 383" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 383" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 383" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 383" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 383" severity error;
			assert reg0b = x"000000ff" report "reg0b error at step 383" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 383" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 383" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 383" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 383" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 383" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 383" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 383" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 383" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 383" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 383" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 383" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 383" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 383" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 383" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 383" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 383" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 383" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 383" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 383" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 383" severity error;
			assert progcounter = x"0000034c" report "progcounter error at step 383" severity error;
			wait for 5 ns;

		-- load instruction 192
			ck <= '0';
			instr <= x"00f695b3"; -- SLL : reg[11] = reg[13] << reg[15] & 0x1f
			wait for 5 ns;
			assert false report "192;0x00f695b3;SLL : reg[11] = reg[13] << reg[15] & 0x1f;OK; ;" severity note;
			assert progcounter = x"0000034c" report "progcounter error at step 384" severity error;
			wait for 5 ns;

		-- execute instruction 192
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 385" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 385" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 385" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 385" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 385" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 385" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 385" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 385" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 385" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 385" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 385" severity error;
			assert reg0b = x"00000100" report "reg0b error at step 385" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 385" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 385" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 385" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 385" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 385" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 385" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 385" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 385" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 385" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 385" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 385" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 385" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 385" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 385" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 385" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 385" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 385" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 385" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 385" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 385" severity error;
			assert progcounter = x"00000350" report "progcounter error at step 385" severity error;
			wait for 5 ns;

		-- load instruction 193
			ck <= '0';
			instr <= x"010595b3"; -- SLL : reg[11] = reg[11] << reg[16] & 0x1f
			wait for 5 ns;
			assert false report "193;0x010595b3;SLL : reg[11] = reg[11] << reg[16] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000350" report "progcounter error at step 386" severity error;
			wait for 5 ns;

		-- execute instruction 193
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 387" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 387" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 387" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 387" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 387" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 387" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 387" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 387" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 387" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 387" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 387" severity error;
			assert reg0b = x"01000000" report "reg0b error at step 387" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 387" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 387" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 387" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 387" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 387" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 387" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 387" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 387" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 387" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 387" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 387" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 387" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 387" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 387" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 387" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 387" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 387" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 387" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 387" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 387" severity error;
			assert progcounter = x"00000354" report "progcounter error at step 387" severity error;
			wait for 5 ns;

		-- load instruction 194
			ck <= '0';
			instr <= x"011595b3"; -- SLL : reg[11] = reg[11] << reg[17] & 0x1f
			wait for 5 ns;
			assert false report "194;0x011595b3;SLL : reg[11] = reg[11] << reg[17] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000354" report "progcounter error at step 388" severity error;
			wait for 5 ns;

		-- execute instruction 194
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 389" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 389" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 389" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 389" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 389" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 389" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 389" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 389" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 389" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 389" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 389" severity error;
			assert reg0b = x"00000000" report "reg0b error at step 389" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 389" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 389" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 389" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 389" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 389" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 389" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 389" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 389" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 389" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 389" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 389" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 389" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 389" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 389" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 389" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 389" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 389" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 389" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 389" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 389" severity error;
			assert progcounter = x"00000358" report "progcounter error at step 389" severity error;
			wait for 5 ns;

		-- load instruction 195
			ck <= '0';
			instr <= x"fff00593"; -- ADDI : reg[11] = reg[00] + -1
			wait for 5 ns;
			assert false report "195;0xfff00593;ADDI : reg[11] = reg[00] + -1;OK; ;" severity note;
			assert progcounter = x"00000358" report "progcounter error at step 390" severity error;
			wait for 5 ns;

		-- execute instruction 195
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 391" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 391" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 391" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 391" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 391" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 391" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 391" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 391" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 391" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 391" severity error;
			assert reg0a = x"0000000f" report "reg0a error at step 391" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 391" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 391" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 391" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 391" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 391" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 391" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 391" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 391" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 391" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 391" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 391" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 391" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 391" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 391" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 391" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 391" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 391" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 391" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 391" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 391" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 391" severity error;
			assert progcounter = x"0000035c" report "progcounter error at step 391" severity error;
			wait for 5 ns;

		-- load instruction 196
			ck <= '0';
			instr <= x"00e5d533"; -- SRL : reg[10] = reg[11] >> reg[14] & 0x1f
			wait for 5 ns;
			assert false report "196;0x00e5d533;SRL : reg[10] = reg[11] >> reg[14] & 0x1f;OK; ;" severity note;
			assert progcounter = x"0000035c" report "progcounter error at step 392" severity error;
			wait for 5 ns;

		-- execute instruction 196
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 393" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 393" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 393" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 393" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 393" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 393" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 393" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 393" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 393" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 393" severity error;
			assert reg0a = x"00000001" report "reg0a error at step 393" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 393" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 393" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 393" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 393" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 393" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 393" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 393" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 393" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 393" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 393" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 393" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 393" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 393" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 393" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 393" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 393" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 393" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 393" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 393" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 393" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 393" severity error;
			assert progcounter = x"00000360" report "progcounter error at step 393" severity error;
			wait for 5 ns;

		-- load instruction 197
			ck <= '0';
			instr <= x"00f5d533"; -- SRL : reg[10] = reg[11] >> reg[15] & 0x1f
			wait for 5 ns;
			assert false report "197;0x00f5d533;SRL : reg[10] = reg[11] >> reg[15] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000360" report "progcounter error at step 394" severity error;
			wait for 5 ns;

		-- execute instruction 197
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 395" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 395" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 395" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 395" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 395" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 395" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 395" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 395" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 395" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 395" severity error;
			assert reg0a = x"00ffffff" report "reg0a error at step 395" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 395" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 395" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 395" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 395" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 395" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 395" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 395" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 395" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 395" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 395" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 395" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 395" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 395" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 395" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 395" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 395" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 395" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 395" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 395" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 395" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 395" severity error;
			assert progcounter = x"00000364" report "progcounter error at step 395" severity error;
			wait for 5 ns;

		-- load instruction 198
			ck <= '0';
			instr <= x"01055533"; -- SRL : reg[10] = reg[10] >> reg[16] & 0x1f
			wait for 5 ns;
			assert false report "198;0x01055533;SRL : reg[10] = reg[10] >> reg[16] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000364" report "progcounter error at step 396" severity error;
			wait for 5 ns;

		-- execute instruction 198
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 397" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 397" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 397" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 397" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 397" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 397" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 397" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 397" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 397" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 397" severity error;
			assert reg0a = x"000000ff" report "reg0a error at step 397" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 397" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 397" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 397" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 397" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 397" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 397" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 397" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 397" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 397" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 397" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 397" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 397" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 397" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 397" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 397" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 397" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 397" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 397" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 397" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 397" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 397" severity error;
			assert progcounter = x"00000368" report "progcounter error at step 397" severity error;
			wait for 5 ns;

		-- load instruction 199
			ck <= '0';
			instr <= x"01155533"; -- SRL : reg[10] = reg[10] >> reg[17] & 0x1f
			wait for 5 ns;
			assert false report "199;0x01155533;SRL : reg[10] = reg[10] >> reg[17] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000368" report "progcounter error at step 398" severity error;
			wait for 5 ns;

		-- execute instruction 199
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 399" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 399" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 399" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 399" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 399" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 399" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 399" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 399" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 399" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 399" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 399" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 399" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 399" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 399" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 399" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 399" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 399" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 399" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 399" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 399" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 399" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 399" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 399" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 399" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 399" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 399" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 399" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 399" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 399" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 399" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 399" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 399" severity error;
			assert progcounter = x"0000036c" report "progcounter error at step 399" severity error;
			wait for 5 ns;

		-- load instruction 200
			ck <= '0';
			instr <= x"40e655b3"; -- SRA: reg[11] = (int) reg[12] >> (int) reg[14] & 0x1f
			wait for 5 ns;
			assert false report "200;0x40e655b3;SRA: reg[11] = (int) reg[12] >> (int) reg[14] & 0x1f;OK; ;" severity note;
			assert progcounter = x"0000036c" report "progcounter error at step 400" severity error;
			wait for 5 ns;

		-- execute instruction 200
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 401" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 401" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 401" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 401" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 401" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 401" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 401" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 401" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 401" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 401" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 401" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 401" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 401" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 401" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 401" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 401" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 401" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 401" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 401" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 401" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 401" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 401" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 401" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 401" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 401" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 401" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 401" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 401" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 401" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 401" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 401" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 401" severity error;
			assert progcounter = x"00000370" report "progcounter error at step 401" severity error;
			wait for 5 ns;

		-- load instruction 201
			ck <= '0';
			instr <= x"40f655b3"; -- SRA: reg[11] = (int) reg[12] >> (int) reg[15] & 0x1f
			wait for 5 ns;
			assert false report "201;0x40f655b3;SRA: reg[11] = (int) reg[12] >> (int) reg[15] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000370" report "progcounter error at step 402" severity error;
			wait for 5 ns;

		-- execute instruction 201
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 403" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 403" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 403" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 403" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 403" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 403" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 403" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 403" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 403" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 403" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 403" severity error;
			assert reg0b = x"ff800000" report "reg0b error at step 403" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 403" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 403" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 403" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 403" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 403" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 403" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 403" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 403" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 403" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 403" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 403" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 403" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 403" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 403" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 403" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 403" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 403" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 403" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 403" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 403" severity error;
			assert progcounter = x"00000374" report "progcounter error at step 403" severity error;
			wait for 5 ns;

		-- load instruction 202
			ck <= '0';
			instr <= x"4105d5b3"; -- SRA: reg[11] = (int) reg[11] >> (int) reg[16] & 0x1f
			wait for 5 ns;
			assert false report "202;0x4105d5b3;SRA: reg[11] = (int) reg[11] >> (int) reg[16] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000374" report "progcounter error at step 404" severity error;
			wait for 5 ns;

		-- execute instruction 202
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 405" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 405" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 405" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 405" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 405" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 405" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 405" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 405" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 405" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 405" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 405" severity error;
			assert reg0b = x"ffffff80" report "reg0b error at step 405" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 405" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 405" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 405" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 405" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 405" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 405" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 405" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 405" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 405" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 405" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 405" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 405" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 405" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 405" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 405" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 405" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 405" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 405" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 405" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 405" severity error;
			assert progcounter = x"00000378" report "progcounter error at step 405" severity error;
			wait for 5 ns;

		-- load instruction 203
			ck <= '0';
			instr <= x"4115d5b3"; -- SRA: reg[11] = (int) reg[11] >> (int) reg[17] & 0x1f
			wait for 5 ns;
			assert false report "203;0x4115d5b3;SRA: reg[11] = (int) reg[11] >> (int) reg[17] & 0x1f;OK; ;" severity note;
			assert progcounter = x"00000378" report "progcounter error at step 406" severity error;
			wait for 5 ns;

		-- execute instruction 203
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 407" severity error;
			assert reg01 = x"00001000" report "reg01 error at step 407" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 407" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 407" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 407" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 407" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 407" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 407" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 407" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 407" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 407" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 407" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 407" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 407" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 407" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 407" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 407" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 407" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 407" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 407" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 407" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 407" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 407" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 407" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 407" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 407" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 407" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 407" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 407" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 407" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 407" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 407" severity error;
			assert progcounter = x"0000037c" report "progcounter error at step 407" severity error;
			wait for 5 ns;

		-- load instruction 204
			ck <= '0';
			instr <= x"00800093"; -- ADDI : reg[01] = reg[00] + 8
			wait for 5 ns;
			assert false report "204;0x00800093;ADDI : reg[01] = reg[00] + 8;OK; ;" severity note;
			assert progcounter = x"0000037c" report "progcounter error at step 408" severity error;
			wait for 5 ns;

		-- execute instruction 204
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 409" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 409" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 409" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 409" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 409" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 409" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 409" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 409" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 409" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 409" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 409" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 409" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 409" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 409" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 409" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 409" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 409" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 409" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 409" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 409" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 409" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 409" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 409" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 409" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 409" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 409" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 409" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 409" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 409" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 409" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 409" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 409" severity error;
			assert progcounter = x"00000380" report "progcounter error at step 409" severity error;
			wait for 5 ns;

		-- load instruction 205
			ck <= '0';
			instr <= x"00008067"; -- JALR : reg[00] = PC+4 and PC = (reg[01] + 0) & ~1 
			wait for 5 ns;
			assert false report "205;0x00008067;JALR : reg[00] = PC+4 and PC = (reg[01] + 0) & ~1;OK; ;" severity note;
			assert progcounter = x"00000380" report "progcounter error at step 410" severity error;
			wait for 5 ns;

		-- execute instruction 205
			ck <= '1';
			wait for 5 ns;
			assert reg00 = x"00000000" report "reg00 error at step 411" severity error;
			assert reg01 = x"00000008" report "reg01 error at step 411" severity error;
			assert reg02 = x"000000bc" report "reg02 error at step 411" severity error;
			assert reg03 = x"fffff800" report "reg03 error at step 411" severity error;
			assert reg04 = x"000007ff" report "reg04 error at step 411" severity error;
			assert reg05 = x"00000001" report "reg05 error at step 411" severity error;
			assert reg06 = x"fffff000" report "reg06 error at step 411" severity error;
			assert reg07 = x"fffffff9" report "reg07 error at step 411" severity error;
			assert reg08 = x"00000007" report "reg08 error at step 411" severity error;
			assert reg09 = x"fffffffa" report "reg09 error at step 411" severity error;
			assert reg0a = x"00000000" report "reg0a error at step 411" severity error;
			assert reg0b = x"ffffffff" report "reg0b error at step 411" severity error;
			assert reg0c = x"80000000" report "reg0c error at step 411" severity error;
			assert reg0d = x"00000001" report "reg0d error at step 411" severity error;
			assert reg0e = x"0000001f" report "reg0e error at step 411" severity error;
			assert reg0f = x"00000008" report "reg0f error at step 411" severity error;
			assert reg10 = x"00000010" report "reg10 error at step 411" severity error;
			assert reg11 = x"00000018" report "reg11 error at step 411" severity error;
			assert reg12 = x"00000205" report "reg12 error at step 411" severity error;
			assert reg13 = x"00000201" report "reg13 error at step 411" severity error;
			assert reg14 = x"00001400" report "reg14 error at step 411" severity error;
			assert reg15 = x"80808080" report "reg15 error at step 411" severity error;
			assert reg16 = x"11223344" report "reg16 error at step 411" severity error;
			assert reg17 = x"fffff000" report "reg17 error at step 411" severity error;
			assert reg18 = x"fffff000" report "reg18 error at step 411" severity error;
			assert reg19 = x"fffff000" report "reg19 error at step 411" severity error;
			assert reg1a = x"fffff000" report "reg1a error at step 411" severity error;
			assert reg1b = x"fffff000" report "reg1b error at step 411" severity error;
			assert reg1c = x"fffff000" report "reg1c error at step 411" severity error;
			assert reg1d = x"fffff000" report "reg1d error at step 411" severity error;
			assert reg1e = x"fffff000" report "reg1e error at step 411" severity error;
			assert reg1f = x"fffff000" report "reg1f error at step 411" severity error;
			assert progcounter = x"00000008" report "progcounter error at step 411" severity error;
			wait for 5 ns;

			wait;
		end process;
END vhdl;