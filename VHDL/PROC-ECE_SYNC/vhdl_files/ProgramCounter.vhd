-- Projet de fin d'études : RISC-V
-- ECE Paris / SECAPEM
-- Program Counter VHDL

-- LIBRARIES
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- ENTITY
entity ProgramCounter is
	port (
		-- INPUTS
		PCclock			: in std_logic;
		PCreset			: in std_logic;
		PCoffset			: in std_logic_vector(31 downto 0);
		PCoffsetsign	: in std_logic;
		PCjal				: in std_logic;
		PCjalr			: in std_logic;
		PCbranch 		: in std_logic;
		PCfunct3			: in std_logic_vector(2 downto 0);
		PCauipc			: in std_logic;
		PCalueq			: in std_logic;
		PCaluinf			: in std_logic;
		PCalusup			: in std_logic;
		PCaluinfU			: in std_logic;
		PCalusupU			: in std_logic;
		
		-- AJOUT
		PCLatchNewPC, PCStopPC			: in std_logic;
		
		-- OUTPUTS
		PCprogcounter	: inout std_logic_vector(31 downto 0);
		PCretAddress	: out std_logic_vector(31 downto 0)
	);
end entity;

-- ARCHITECTURE
architecture archi of ProgramCounter is

	-- AJOUT 
	
	signal SigBranchCond : std_logic;
	signal SigMux1Sel 	: std_logic;
	signal SigMux2Sel 	: std_logic;
	signal SigMux1Out 	: std_logic_vector(31 downto 0);
	signal SigMux2Out 	: std_logic_vector(31 downto 0);
	signal SigOffSum 		: std_logic_vector(31 downto 0);
	signal SigOffSub 		: std_logic_vector(31 downto 0);
	signal SigRegPC, SigPC, SigMuxRegPC : std_logic_vector(31 downto 0);
	signal SigRegReset1, SigRegReset2 : std_logic;
	
	
begin
	-- BEGIN
	
	-- reset
	SigRegReset1 <= '1' when PCreset='1' else PCreset when rising_edge(PCclock);
	SigRegReset2 <= '1' when PCreset='1' else SigRegReset1 when rising_edge(PCclock);
	
	-- branch cond
	SigBranchCond <= 		PCalueq 		when PCfunct3 = "000" 
							else not PCalueq 	when PCfunct3 = "001"
							else '0' 		when PCfunct3 = "010"
							else '0' 		when PCfunct3 = "011"
							else PCaluinf 		when PCfunct3 = "100"
							else PCalusup 		when PCfunct3 = "101"
							else PCaluinfU 		when PCfunct3 = "110"
							else PCalusupU 		when PCfunct3 = "111"
							else '0';
							
	-- mux 1
	SigMux1Sel <= 	(SigBranchCond AND PCbranch) OR PCjal OR PCjalr OR PCauipc; --mux3
	SigMux1Out <= 	"00000000000000000000000000000100" when SigMux1Sel = '0'
					else PCoffset;
	
	-- adder
	SigOffSum <= 	std_logic_vector(unsigned(SigRegPC) + unsigned(SigMux1Out));
	SigOffSub <= 	std_logic_vector(unsigned(SigRegPC) - unsigned(SigMux1Out));
	
	-- mux 2
	SigMux2Sel <= 	SigMux1Sel AND PCoffsetsign;
	
	SigMux2Out <= 	SigRegPC when PCStopPC='1' else
					(others => '0') when SigRegReset2='1' else
					PCoffset when PCjalr = '1' else
					SigOffSum when (SigMux2Sel = '0' AND PCjalr = '0') OR PCjal = '1' OR PCbranch = '1' else
					SigOffSub;
			
	SigMuxRegPC <= SigPC when PCLatchNewPC='1' else SigRegPC;		
	SigRegPC <= (others => '0') when PCreset='1' else SigMuxRegPC when rising_edge(PCclock);
	
	PCprogcounter <= SigPC;
	SigPC <= SigMux2Out;
	PCretAddress <= std_logic_vector(unsigned(SigRegPC)+4);
							
	-- END
end archi;
-- END FILE

